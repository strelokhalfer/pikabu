const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'content-script': './src/content-script/index.ts',
        popup: './src/popup/index.tsx',
        background: './src/background/index.ts',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /src\/assest\/.*/,
                type: 'asset/resource',
            },
            {
                test: /\.s[ac]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    mode: 'development',
    devtool: false,
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: 'src/assets',
                    to: '.',
                },
                {
                    from: 'src/popup/popup.html',
                    to: './popup.html',
                },
            ],
        }),
    ],
};
