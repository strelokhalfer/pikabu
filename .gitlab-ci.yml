image: node:18-alpine

stages:
  - install
  - build
  - test
  - version
  - release


# Основные условия для прохождения пайплайна:
# - Для MR - никогда (нет необходимости, так как запускается на каждой ветке)
# - Для дефолтной (master) ветки с релизным коммитом - никогда (нет необходимости, так как в пайплайне по релизному тегу происходит тоже самое + релиз)
# - в остальных случаях всегда
.base_conditions:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\)/
      when: never
    - when: always


# Условия для поднятия версии с созданием тега (должна быть дефолтная master ветка, но не релизный коммит)
.version_conditions:
  when: manual
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE !~ /^chore\(release\)/


# Условия для релиза (ожидаем тег версии "v*" и коммит "chore(release)*")
.release_conditions:
  rules:
    - if: $CI_COMMIT_TAG =~ /^v/ && $CI_COMMIT_MESSAGE =~ /^chore\(release\)/


# Установка npm зависимостей
install:
  extends: .base_conditions
  stage: install
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - .npm/
  script:
    - npm ci --cache .npm --prefer-offline --no-audit
  artifacts:
    paths:
      - node_modules
      - .angular


# Сборка npm пакетов
build:
  extends: .base_conditions
  needs:
    - job: install
      artifacts: true
  stage: build
  script:
    - npm run ci:build
  artifacts:
    paths:
      - dist


# Unit-tests
test:
  extends: .base_conditions
  stage: test
  needs:
    - job: install
      artifacts: true
    - job: build
      artifacts: true
  script:
    - npm run ci:test
  artifacts:
    when: always
    reports:
      junit:
        - junit.xml


# Настройка гита для коммита и пуша
.version:
  image: node:18 # в alpine нет git
  extends: .version_conditions
  resource_group: version
  stage: version
  needs:
    - job: test
    - job: build
      artifacts: true
  before_script:
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git remote set-url --push origin https://${GITLAB_USER_LOGIN}:${CI_GIT_TOKEN}@${CI_REPOSITORY_URL#*@}
  after_script:
    - git push --follow-tags origin HEAD:$CI_COMMIT_REF_NAME


# Автоматическое поднятие версии (создается git тег, который инициирует публикацию релиза)
version:release:
  extends: .version
  script:
    - npx standard-version -t v --commit-all


release:
  image: debian:12.1-slim
  extends: .release_conditions
  stage: release
  needs:
    - job: test
    - job: build
      artifacts: true
  before_script:
    - apt-get update && apt-get install -y zip curl jq
    - curl --location --output /usr/local/bin/release-cli "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64"
    - chmod +x /usr/local/bin/release-cli
  script:
    - DIST_CHROME_FILE_NAME="pikabu-chrome-extension.$CI_COMMIT_TAG.zip"
    - DIST_FF_FILE_NAME="pikabu-ff-extension.$CI_COMMIT_TAG.zip"
    - cd ./dist/
    - zip -r ../$DIST_CHROME_FILE_NAME ./
    - mv -f manifest.ff.json manifest.json
    - zip -r ../$DIST_FF_FILE_NAME ./
    - cd ../
    - UPLOAD_CHROME_RESULT=$(curl --fail --request POST --header "PRIVATE-TOKEN:$CI_UPLOAD_TOKEN" --form "file=@$DIST_CHROME_FILE_NAME" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads")
    - UPLOAD_FF_RESULT=$(curl --fail --request POST --header "PRIVATE-TOKEN:$CI_UPLOAD_TOKEN" --form "file=@$DIST_FF_FILE_NAME" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads")
    - UPLOAD_CHROME_URL=$(echo $UPLOAD_CHROME_RESULT | jq -r '.full_path')
    - UPLOAD_FF_URL=$(echo $UPLOAD_FF_RESULT | jq -r '.full_path')
    - release-cli create --description "$CI_COMMIT_TAG" --tag-name "$CI_COMMIT_TAG" --assets-link "{\"url\":\"https://gitlab.com$UPLOAD_CHROME_URL\",\"name\":\"pikabu-chrome-extension.zip\"}" --assets-link "{\"url\":\"https://gitlab.com$UPLOAD_FF_URL\",\"name\":\"pikabu-ff-extension.zip\"}"