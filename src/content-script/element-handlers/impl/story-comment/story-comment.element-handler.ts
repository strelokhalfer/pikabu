import { ElementHandler } from '../../element-handler';
import { MobileCommentModel } from '../../../../background/services/mobile-api/models/mobile-comment.model';
import { debounceTime, filter, fromEvent, map, merge, Observable, shareReplay, startWith, switchMap, take, tap } from 'rxjs';
import { Container } from 'typedi';
import { MutationsService } from '../../../services/mutations.service';
import { setHint } from '../../../dom-helpers/set-hint';

import './story-comment.scss';
import { isContainSelector } from '../../../dom-helpers/is-contain-selector';
import { createFromHtml } from '../../../dom-helpers/create-from-html';
import { LoaderHtmlConst } from '../../../consts/loader/loader-html.const';
import { HandlersService } from '../../../services/handlers.service';
import { UserInfoElementHandler } from '../user-info/user-info.element-handler';

export class StoryCommentElementHandler extends ElementHandler {
    static selector = '.comment';

    readonly originSelectors = {
        otherRatingCount: '.comment__rating-count:not(:first-child)',
        ratingUp: '.comment__rating-up',
        ratingDown: '.comment__rating-down',
    };

    readonly needDomChanges$ = this.getIsValid$().pipe(
        filter(Boolean),
        take(1),
        switchMap(() => this.domChanges()),
        shareReplay({
            bufferSize: 1,
            refCount: true,
        }),
    );

    getId(): number {
        return Number(this.element.getAttribute('data-id'));
    }

    getIsValid(): boolean {
        const selectors = Object.values(this.originSelectors);
        return selectors.every((selector) => isContainSelector(this.element, selector));
    }

    getIsValid$(): Observable<boolean> {
        return Container.get(MutationsService)
            .getTreeMutations(this.element)
            .pipe(
                debounceTime(0),
                startWith(null),
                map(() => this.getIsValid()),
            );
    }

    getUserInfo() {
        return this.getIsValid$().pipe(
            filter(Boolean),
            take(1),
            switchMap(() => Container.get(HandlersService).listenOne(UserInfoElementHandler, this.element)),
        );
    }

    setCommentData(data?: MobileCommentModel): Observable<any> {
        return this.needDomChanges$.pipe(
            filter(Boolean),
            tap(() => this.setData(data)),
        );
    }

    listenRatingChange(): Observable<any> {
        return this.needDomChanges$.pipe(
            filter(Boolean),
            switchMap(() =>
                merge(
                    fromEvent(this.element.querySelector(this.originSelectors.ratingUp)!, 'click'),
                    fromEvent(this.element.querySelector(this.originSelectors.ratingDown)!, 'click'),
                ),
            ),
        );
    }

    protected domChanges(): Observable<boolean> {
        return new Observable<any>((subscriber) => {
            subscriber.next(this.tryChange());
            return () => this.tryRevert();
        });
    }

    protected tryChange(): boolean {
        const originCountElement = this.element.querySelector(this.originSelectors.otherRatingCount);
        const upButtonElement = this.element.querySelector(this.originSelectors.ratingUp);
        const downButtonElement = this.element.querySelector(this.originSelectors.ratingDown);

        if (!originCountElement || !upButtonElement || !downButtonElement) {
            return false;
        }

        originCountElement.classList.add('pp--comment__rating-count_plus');
        upButtonElement.classList.add('pp--comment__rating-up');

        if (this.element.querySelector('.pp--comment__rating-count')) {
            return true; // todo: не должно сюда заходить
        }

        const customCountElement = document.createElement('div');
        customCountElement.classList.add('pp--comment__rating-count');

        const loaderElement = createFromHtml(LoaderHtmlConst);
        customCountElement.appendChild(loaderElement);

        setHint(customCountElement, `Рейтинг загружается`);

        downButtonElement.parentElement!.insertBefore(customCountElement, downButtonElement);
        return true;
    }

    protected tryRevert(): void {
        let customCountElement = this.element.querySelector('.pp--comment__rating-count');
        customCountElement?.remove();

        const originCountElement = this.element.querySelector(this.originSelectors.otherRatingCount);
        const upButtonElement = this.element.querySelector(this.originSelectors.ratingUp);

        if (!originCountElement || !upButtonElement) {
            return;
        }

        originCountElement.classList.remove('pp--comment__rating-count_plus');
        upButtonElement.classList.remove('pp--comment__rating-up');
    }

    protected setData(data?: MobileCommentModel | null): void {
        const originCountElement = this.element.querySelector(this.originSelectors.otherRatingCount);
        const customCountElement = this.element.querySelector('.pp--comment__rating-count');

        if (!customCountElement || !originCountElement) {
            return;
        }

        customCountElement.innerHTML = ''; // remove loader

        const originalCount = originCountElement.textContent || null;

        if (data && data.comment_rating !== null) {
            customCountElement.textContent = data.comment_rating + '';
            setHint(customCountElement, `+${data.comment_pluses} / -${data.comment_minuses}`);
        } else {
            customCountElement.textContent = `?`;
            setHint(customCountElement, `Суммарный рейтинг скрыт (плюсов:\u00A0${originalCount ?? 'нет'})`);
        }
    }
}
