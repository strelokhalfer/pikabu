import { ElementHandler } from '../element-handler';

export class UserElementHandler extends ElementHandler {
    static selector = '.user_right-sidebar';

    getNick(): string | null {
        const nickElement = this.element.querySelector('.user__nick');

        return nickElement?.textContent ?? null;
    }
}
