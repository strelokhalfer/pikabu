import { ElementHandler } from '../../element-handler';
import { filter, fromEvent, Observable, share, shareReplay, tap } from 'rxjs';
import { MobileStoryModel } from '../../../../background/services/mobile-api/models/mobile-story.model';

import './story-mobile-vote.scss';
import { setHint } from '../../../dom-helpers/set-hint';

export class StoryMobileVoteElementHandler extends ElementHandler {
    static selector = '.story__footer-rating';

    readonly needDomModify$ = this.modifyDomOnSubscribe().pipe(
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );

    upButtonEvent(eventName: string) {
        const element = this.element.querySelector('.story__rating-plus');
        return fromEvent(element!, eventName);
    }

    downButtonEvent(eventName: string) {
        const element = this.element.querySelector('.story__rating-down');
        return fromEvent(element!, eventName);
    }

    updateRating(story: MobileStoryModel | null): Observable<any> {
        return this.needDomModify$.pipe(
            filter(Boolean),
            tap(() => this.setData(story)),
        );
    }

    private modifyDomOnSubscribe(): Observable<boolean> {
        return new Observable<any>((subscriber) => {
            subscriber.next(this.modifyDom());
            return () => this.revertDomChanges();
        });
    }

    private modifyDom(): boolean {
        const ratingUpElement = this.element.querySelector('.story__rating-plus');
        const ratingDownElement = this.element.querySelector('.story__rating-down');

        if (!ratingUpElement || !ratingDownElement) {
            return false;
        }

        const ratingUpCountElement = ratingUpElement.querySelector('.story__rating-count');

        if (!ratingUpCountElement) {
            return false;
        }

        ratingUpCountElement.classList.add('pp--story__rating-count_plus-mobile');

        const ratingSumCountElement = document.createElement('div');
        ratingSumCountElement.classList.add('story__rating-count', 'pp--story__rating-count_sum-mobile');
        setHint(ratingSumCountElement, 'Рейтинг с учетом минусов');

        ratingDownElement!.parentElement!.insertBefore(ratingSumCountElement, ratingDownElement);

        return true;
    }

    private setData(story: MobileStoryModel | null): void {
        const ratingUpCountElement = this.element.querySelector('.pp--story__rating-count_plus-mobile') as HTMLElement;
        const ratingSumCountElement = this.element.querySelector('.pp--story__rating-count_sum-mobile') as HTMLElement;

        if (!ratingUpCountElement || !ratingSumCountElement) {
            return;
        }

        const originalCount = ratingUpCountElement.textContent || null;

        if (story && story.story_digs !== null) {
            ratingSumCountElement.textContent = (story.story_digs ?? 0) + '';
            setHint(ratingSumCountElement, `+${story.story_pluses} / -${story.story_minuses}`);
        } else {
            ratingSumCountElement.textContent = `?`;
            setHint(ratingSumCountElement, `Суммарный рейтинг скрыт (плюсов:\u00A0${originalCount ?? 'нет'})`);
        }
    }

    private revertDomChanges() {
        const ratingUpCountElement = this.element.querySelector('.pp--story__rating-count_plus-mobile');
        const ratingSumCountElement = this.element.querySelector('.pp--story__rating-count_sum-mobile');

        if (!ratingUpCountElement || !ratingSumCountElement) {
            return;
        }
        ratingUpCountElement.classList.remove('pp--story__rating-count_plus-mobile');
        ratingSumCountElement.remove();
    }
}
