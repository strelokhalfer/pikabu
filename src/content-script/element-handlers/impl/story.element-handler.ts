import { ElementHandler } from '../element-handler';
import { StoryToolbarElementHandler } from './story-toolbar.element-handler';
import {
    catchError,
    combineLatest,
    defer,
    EMPTY,
    from,
    map,
    merge,
    Observable,
    of,
    repeat,
    share,
    shareReplay,
    startWith,
    Subject,
    switchMap,
    tap,
} from 'rxjs';
import { StoryVoteElementHandler } from './story-vote/story-vote.element-handler';
import { Container } from 'typedi';
import { CommandService } from '../../../common/services/command/command.service';
import { CommandEnum } from '../../../common/enums/command.enum';
import { StoryAdditionalToolbarElementHandler } from './story-additional-toolbar/story-additional-toolbar.element-handler';
import { MobileStoryResponseModel } from '../../../background/services/mobile-api/models/mobile-story-response.model';
import { StoryMobileVoteElementHandler } from './story-mobile-vote/story-mobile-vote.element-handler';
import { EventEnum } from '../../../common/enums/event.enum';
import { EventsService } from '../../../common/services/events/events.service';
import { StoryCommentElementHandler } from './story-comment/story-comment.element-handler';
import { HandlersService } from '../../services/handlers.service';
import { switchEvery } from '../../../common/rxjs-operators/switch-every/switch-every';
import { UserClientService } from '../../../common/services/user/user-client.service';
import { UserInfoElementHandler } from './user-info/user-info.element-handler';

export class StoryElementHandler extends ElementHandler {
    static selector = 'article.story';

    private readonly dataChanged$ = new Subject<void>();

    private readonly data$ = defer(() =>
        Container.get(CommandService).execute<number, MobileStoryResponseModel>(CommandEnum.GetStoryData, this.getId()),
    ).pipe(
        repeat({
            delay: () => this.dataChanged$,
        }),
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );

    private readonly dataAllComments$ = defer(() =>
        Container.get(CommandService).execute<number, MobileStoryResponseModel>(CommandEnum.GetStoryDataAllComments, this.getId()),
    ).pipe(
        repeat({
            delay: () => this.dataChanged$,
        }),
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );

    getId(): number {
        const idString = this.element.getAttribute('data-story-id');
        return Number(idString);
    }

    getAuthorId(): number {
        const idString = this.element.getAttribute('data-author-id');
        return Number(idString);
    }

    getAuthorName(): string | null {
        return this.element.getAttribute('data-author-name');
    }

    getToolbar() {
        return Container.get(HandlersService).listenOne(StoryToolbarElementHandler, this.element);
    }

    getAdditionalToolbar() {
        return Container.get(HandlersService).listenOne(StoryAdditionalToolbarElementHandler, this.element);
    }

    getVote() {
        return Container.get(HandlersService).listenOne(StoryVoteElementHandler, this.element);
    }

    getMobileVote() {
        return Container.get(HandlersService).listenOne(StoryMobileVoteElementHandler, this.element);
    }

    getComments() {
        const handlersService = Container.get(HandlersService);

        const scope = document.querySelector(`.comments[data-story-id="${this.getId()}"]`);

        if (!scope) {
            return of([]);
        }

        return handlersService.listenAll(StoryCommentElementHandler, scope);
    }

    getUsers() {
        const storyUser$ = Container.get(HandlersService).listenOne(UserInfoElementHandler, this.element);

        return this.getComments().pipe(
            switchEvery((comment) => comment.getUserInfo()),
            map((comments) => comments.filter(Boolean) as UserInfoElementHandler[]),
            switchMap((commentUser) => storyUser$.pipe(map((storyUser) => [storyUser, ...commentUser]))),
            shareReplay({
                refCount: true,
                bufferSize: 1,
            }),
        );
    }

    showCommentsRating(): Observable<any> {
        const comments$ = this.getComments();

        return merge(
            comments$.pipe(
                switchMap((comments) => {
                    if (!comments.length) {
                        return EMPTY;
                    }

                    return this.dataAllComments$.pipe(
                        switchMap((data) => {
                            const commentsDataMap = new Map(data.comments.map((comment) => [comment.comment_id, comment]));

                            return comments$.pipe(switchEvery((comment) => comment.setCommentData(commentsDataMap.get(comment.getId()))));
                        }),
                    );
                }),
            ),
            this.listenCommentRatingChange(),
        );
    }

    listenCommentRatingChange(): Observable<any> {
        return this.getComments().pipe(
            switchEvery((comment) => comment.listenRatingChange()),
            tap(() => this.dataChanged$.next()),
        );
    }

    listenRatingChange(): Observable<any> {
        const eventsService = Container.get(EventsService);

        return this.getVote().pipe(
            switchMap((vote) =>
                merge(
                    vote.upButtonEvent('click').pipe(switchMap(() => eventsService.emitEvent(EventEnum.StoryUpvote, this.getId()))),
                    vote.downButtonEvent('click').pipe(switchMap(() => eventsService.emitEvent(EventEnum.StoryDownvote, this.getId()))),
                ),
            ),
            tap(() => this.dataChanged$.next()),
        );
    }

    showIgnoreButton(): Observable<any> {
        const authorId = this.getAuthorId();

        return this.getToolbar().pipe(switchMap((toolbar) => toolbar.showIgnoreButton(authorId)));
    }

    showUserRegistrationDateIndicator(): Observable<any> {
        const userService = Container.get(UserClientService);

        const users$ = this.getUsers();

        const modifyDom$ = users$.pipe(switchEvery((user) => user.needDomModify$));

        const fillData$ = users$.pipe(
            switchEvery((user) =>
                from(userService.getShortUser(user.getId(), user.getName())).pipe(
                    catchError(() => {
                        user.updateInfo(null);
                        return EMPTY;
                    }),
                    switchMap((userData) => user.updateInfo(userData)),
                ),
            ),
        );

        return merge(modifyDom$, fillData$);
    }

    showRating(): Observable<any> {
        const allRatingHandlers$ = combineLatest([
            this.getVote().pipe(startWith(null)),
            this.getMobileVote().pipe(startWith(null)),
            this.getAdditionalToolbar().pipe(startWith(null)),
        ]).pipe(
            map((elements) => elements.filter(Boolean)),
            share(),
        );

        const allDomModify$ = allRatingHandlers$.pipe(switchEvery((element) => element!.needDomModify$));

        const allUpdate$ = allRatingHandlers$.pipe(
            switchEvery((element) =>
                this.data$.pipe(
                    catchError(() => element!.updateRating(null)),
                    switchMap((data) => element!.updateRating(data.story)),
                ),
            ),
        );

        return merge(allDomModify$, allUpdate$);
    }
}
