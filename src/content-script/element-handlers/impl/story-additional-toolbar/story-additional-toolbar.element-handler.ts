import { ElementHandler } from '../../element-handler';
import { MobileStoryModel } from '../../../../background/services/mobile-api/models/mobile-story.model';
import { createFromHtml } from '../../../dom-helpers/create-from-html';
import { setHint } from '../../../dom-helpers/set-hint';
import { filter, Observable, share, shareReplay, tap } from 'rxjs';

import './story-additional-toolbar.scss';
import { RatingHtmlConst } from './rating/rating-html.const';

export class StoryAdditionalToolbarElementHandler extends ElementHandler {
    static selector = '.story__additional-tools';

    readonly needDomModify$ = this.modifyDomOnSubscribe().pipe(
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );

    updateRating(story: MobileStoryModel | null): Observable<any> {
        return this.needDomModify$.pipe(
            filter(Boolean),
            tap(() => this.setData(story)),
        );
    }

    private modifyDomOnSubscribe(): Observable<boolean> {
        return new Observable<any>((subscriber) => {
            subscriber.next(this.modifyDom());
            return () => this.revertDomChanges();
        });
    }

    private modifyDom(): boolean {
        this.element.classList.add('pp--story__additional-tools');

        const ratingElement = createFromHtml(RatingHtmlConst);
        this.element.appendChild(ratingElement);
        ratingElement.classList.add('pp--disabled');

        return true;
    }

    private setData(story: MobileStoryModel | null): void {
        const ratingElement = this.element.querySelector('.page-story__rating-block');
        if (!ratingElement) {
            return;
        }

        if (!story) {
            ratingElement.classList.add('pp--disabled');
            setHint(
                ratingElement,
                'Не удалось загрузить рейтинг. Это может быть связано с ограничениями, применяемыми к NSFW или политическим постам.',
            );
            return;
        }

        const upVoteCountElement = ratingElement.querySelector('.icon--ui__rating-up + .page-story__rating-vote');

        if (upVoteCountElement) {
            upVoteCountElement.textContent = String(story.story_pluses ?? '?');
        }

        const downVoteCountElement = ratingElement.querySelector('.icon--ui__rating-down + .page-story__rating-vote');

        if (downVoteCountElement) {
            downVoteCountElement.textContent = String(story.story_minuses ?? '?');
        }

        const storyRatingElement = ratingElement.querySelector('.page-story__rating');

        if (!storyRatingElement) {
            return;
        }

        const storyRatingPlusElement = storyRatingElement.querySelector('.page-story__rating-pluses') as HTMLElement;

        if (!storyRatingPlusElement) {
            return;
        }

        if (story.story_digs === null) {
            ratingElement.classList.add('pp--disabled');
            setHint(ratingElement, 'Рейтинг скрыт, так как действует защита автора (по критериям пикабу)');
        } else {
            ratingElement.classList.remove('pp--disabled');
            setHint(ratingElement, null);
        }

        if (story.story_pluses !== null && story.story_minuses !== null) {
            const sum = story.story_pluses + story.story_minuses;
            const relative = story.story_pluses / sum;
            const plusesPercent = relative * 100;

            storyRatingElement.setAttribute('data-pluses', String(story.story_pluses));
            storyRatingElement.setAttribute('data-minuses', String(story.story_minuses));
            storyRatingPlusElement.setAttribute('data-width', String(plusesPercent));
            storyRatingPlusElement.style.width = `${plusesPercent}%`;
        }
    }

    private revertDomChanges() {
        this.element.classList.remove('pp--story__additional-tools');

        const ratingElement = this.element.querySelector('.page-story__rating-block');
        ratingElement?.remove();
    }
}
