import './rating.scss';

export const RatingHtmlConst = `<div class='page-story__rating-block pp-page-story__rating-block'>
    <div class='page-story__rating-votes pp--page-story__rating-votes'>
        <svg xmlns='http://www.w3.org/2000/svg'
             class='icon icon--ui__rating-up icon--ui__rating-up_page-story icon--ui__rating-up_active'>
            <use xlink:href='#icon--ui__rating-up'></use>
        </svg>
        <span class='page-story__rating-vote'>0</span>
        <span>/</span>
        <svg xmlns='http://www.w3.org/2000/svg'
             class='icon icon--ui__rating-down icon--ui__rating-down_page-story icon--ui__rating-down_active'>
            <use xlink:href='#icon--ui__rating-down'></use>
        </svg>
        <span class='page-story__rating-vote'>0</span>
    </div>
    <div class='page-story__rating' data-pluses='0' data-minuses='0'>
        <div class='page-story__rating-pluses' data-width='50' style='width: 50%'></div>
    </div>
</div>`;
