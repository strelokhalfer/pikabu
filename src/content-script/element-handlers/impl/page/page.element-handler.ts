import { ElementHandler } from '../../element-handler';
import { Observable } from 'rxjs';

import './page.scss';

export class PageElementHandler extends ElementHandler {
    static selector = '.app';

    hideDonates(): Observable<any> {
        return new Observable<any>(() => {
            this.element.classList.add('pp--hide-donates');
            return () => this.element.classList.remove('pp--hide-donates');
        });
    }

    hideDesignShortStory(): Observable<any> {
        return new Observable<any>(() => {
            this.element.classList.add('pp--hide-design-short-story');
            return () => this.element.classList.remove('pp--hide-design-short-story');
        });
    }
}
