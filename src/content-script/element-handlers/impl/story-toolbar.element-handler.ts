import { ElementHandler } from '../element-handler';
import { catchError, EMPTY, merge, Observable, switchMap, take, tap } from 'rxjs';
import { Container } from 'typedi';
import { CommandService } from '../../../common/services/command/command.service';
import { ToolbarButtonElementHandler } from './toolbar-button/toolbar-button.element-handler';
import { IconEnum } from '../../enums/icon.enum';
import { CommandEnum } from '../../../common/enums/command.enum';

export class StoryToolbarElementHandler extends ElementHandler {
    static selector = '.story__tools, .story__footer-tools-inner .story__footer-separator';

    private ignored = false;

    showIgnoreButton(authorId: number): Observable<any> {
        const commandService = Container.get(CommandService);
        const button = new ToolbarButtonElementHandler(IconEnum.EyeClose);

        const init = () => {
            button.setActive(true);
            button.setHint('Забанить посты пользователя навсегда');
        };

        const active = () => {
            button.setActive(false);
            button.setIcon(IconEnum.Preloader, true);
            button.setHint('Баним посты пользователя');
        };

        const done = () => {
            button.setIcon(IconEnum.EyeClose);
            button.setHint('Посты пользователя успешно забанены');
            this.ignored = true;
        };

        const action$ = button.event('click').pipe(
            take(1),
            tap(active),
            switchMap(() => commandService.execute(CommandEnum.IgnoreForever, authorId)),
            catchError(() => {
                init();
                return EMPTY;
            }),
            tap(done),
        );

        const mount$ = new Observable(() => {
            this.element.appendChild(button.element);

            return () => this.element.removeChild(button.element);
        });

        if (!this.ignored) {
            init();
        } else {
            done();
        }

        return merge(mount$, action$);
    }
}
