import { ElementHandler } from '../../element-handler';
import { IconEnum } from '../../../enums/icon.enum';

import './toolbar-button.scss';

export class ToolbarButtonElementHandler extends ElementHandler {
    constructor(icon: IconEnum) {
        super(document.createElement('div'));

        this.element.classList.add('pp--story__tool-button');

        this.setIcon(icon);
    }

    setActive(active: boolean): void {
        this.element.classList.toggle('pp--story__tool-button_active', active);
    }

    setIcon(icon: IconEnum, rotation = false): void {
        this.element.classList.toggle('pp--rotation', rotation);
        this.element.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="icon" style="width: 16px;height: 16px;"><use xlink:href="#${icon}"></use></svg>`;
    }

    setHint(hint: string | null): void {
        this.element.classList.toggle('hint', !!hint);
        this.element.setAttribute('aria-label', hint ?? '');
    }
}
