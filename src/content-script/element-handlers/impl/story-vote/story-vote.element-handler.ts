import { ElementHandler } from '../../element-handler';
import { filter, fromEvent, Observable, share, shareReplay, tap } from 'rxjs';
import { MobileStoryModel } from '../../../../background/services/mobile-api/models/mobile-story.model';
import { setHint } from '../../../dom-helpers/set-hint';

import './story-vote.scss';
import { createFromHtml } from '../../../dom-helpers/create-from-html';
import { LoaderHtmlConst } from '../../../consts/loader/loader-html.const';

export class StoryVoteElementHandler extends ElementHandler {
    static selector = '.story__rating-block';

    readonly needDomModify$ = this.modifyDomOnSubscribe().pipe(
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );

    upButtonEvent(eventName: string) {
        const element = this.element.querySelector('.story__rating-plus');
        return fromEvent(element!, eventName);
    }

    downButtonEvent(eventName: string) {
        const element = this.element.querySelector('.story__rating-down');
        return fromEvent(element!, eventName);
    }

    updateRating(story: MobileStoryModel | null): Observable<any> {
        return this.needDomModify$.pipe(
            filter(Boolean),
            tap(() => this.setData(story)),
        );
    }

    private modifyDomOnSubscribe(): Observable<boolean> {
        return new Observable<any>((subscriber) => {
            subscriber.next(this.modifyDom());
            return () => this.revertDomChanges();
        });
    }

    private modifyDom(): boolean {
        const ratingUpElement = this.element.querySelector('.story__rating-plus');
        const ratingDownElement = this.element.querySelector('.story__rating-down');

        if (!ratingUpElement || !ratingDownElement) {
            return false;
        }

        const ratingUpCountElement = ratingUpElement?.querySelector('.story__rating-count');

        if (!ratingUpCountElement) {
            return false;
        }

        ratingUpCountElement.classList.add('pp--story__rating-count_plus');

        const ratingSumCountElement = document.createElement('div');
        ratingSumCountElement.classList.add('story__rating-count', 'pp--story__rating-count_sum');
        ratingSumCountElement.appendChild(createFromHtml(LoaderHtmlConst));

        setHint(ratingSumCountElement, 'Суммарный рейтинг загружается');

        this.element.insertBefore(ratingSumCountElement, ratingDownElement);

        return true;
    }

    private setData(story: MobileStoryModel | null): void {
        const ratingUpCountElement = this.element.querySelector('.pp--story__rating-count_plus');
        const ratingSumCountElement = this.element.querySelector('.pp--story__rating-count_sum') as HTMLElement;

        if (!ratingUpCountElement || !ratingSumCountElement) {
            return;
        }

        ratingSumCountElement.innerHTML = ''; // remove loader

        if (!story) {
            ratingSumCountElement.textContent = 'x';
            setHint(ratingSumCountElement, 'Не удалось загрузить информацию');
            return;
        }

        if (story.story_digs !== null) {
            if (ratingUpCountElement) {
                ratingUpCountElement.textContent = story.story_pluses + '';
            }

            ratingSumCountElement.textContent = (story.story_digs ?? 0) + '';
            setHint(ratingSumCountElement, 'Суммарный рейтинг с учетом минусов');

            ratingSumCountElement.style.display = '';
        } else {
            ratingSumCountElement.style.display = 'none';
        }
    }

    private revertDomChanges() {
        const ratingUpCountElement = this.element.querySelector('.pp--story__rating-count_plus');
        const ratingSumCountElement = this.element.querySelector('.pp--story__rating-count_sum');

        if (!ratingUpCountElement || !ratingSumCountElement) {
            return;
        }
        ratingUpCountElement.classList.remove('pp--story__rating-count_plus');
        ratingSumCountElement.remove();
    }
}
