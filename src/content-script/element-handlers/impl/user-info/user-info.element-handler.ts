import { ElementHandler } from '../../element-handler';
import { filter, Observable, shareReplay, tap } from 'rxjs';

import { UserShortModel } from '../../../../common/services/user/user-short.model';
import { createFromHtml } from '../../../dom-helpers/create-from-html';
import { LoaderHtmlConst } from '../../../consts/loader/loader-html.const';
import { setHint } from '../../../dom-helpers/set-hint';
import { sub } from 'date-fns';

import './user-info.scss';

const inStoryMobile = 'html.mv .story__main .story__author-header .story__user-link';
const inStoryCommMobile = 'html.mv .story__author-panel .story__user-info .story__user-link';
const inStoryDesktop = 'html.dv .story__user-info .story__user-link';
const inStoryCommDesktop = 'html.dv .story__author-panel .story__user-info .story__user-link';
const inComment = '.comment__user';

export class UserInfoElementHandler extends ElementHandler {
    static selector = [inStoryCommMobile, inStoryDesktop, inStoryMobile, inStoryCommDesktop, inComment].join(', ');

    readonly needDomModify$ = this.modifyDomOnSubscribe().pipe(
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );
    private additionalInfoElement?: Element;

    getId(): number | null {
        let idStr = this.element.getAttribute('data-id');

        if (!idStr) {
            idStr = this.element.parentElement?.parentElement?.querySelector('[data-id]')?.getAttribute('data-id') ?? null;
        }

        return idStr ? Number(idStr) : null;
    }

    getName(): string | null {
        const name = this.getRawName();

        if (name === 'DELETED' || name === 'Аноним') {
            return null;
        }

        return name;
    }

    private getRawName(): string | null {
        return this.element.getAttribute('data-name') || this.element.textContent?.trim() || null;
    }

    updateInfo(user: UserShortModel | null): Observable<any> {
        return this.needDomModify$.pipe(
            filter(Boolean),
            tap(() => this.setData(user)),
        );
    }

    private modifyDomOnSubscribe(): Observable<boolean> {
        return new Observable<any>((subscriber) => {
            subscriber.next(this.modifyDom());
            return () => this.revertDomChanges();
        });
    }

    private modifyDom(): boolean {
        if (this.additionalInfoElement) {
            return true;
        }

        this.additionalInfoElement = createFromHtml('<div class="pp--additional-user-info"></div>');

        const loader = createFromHtml(LoaderHtmlConst);
        this.additionalInfoElement.appendChild(loader);
        setHint(this.additionalInfoElement, 'Загружаем информацию о пользователеле');

        if (this.element.nextSibling) {
            this.element.parentElement!.insertBefore(this.additionalInfoElement, this.element.nextSibling);
        } else {
            this.element.parentElement!.appendChild(this.additionalInfoElement);
        }

        return true;
    }

    private setData(user: UserShortModel | null): void {
        if (!this.additionalInfoElement) {
            return;
        }

        setHint(this.additionalInfoElement, null);
        this.additionalInfoElement.innerHTML = ''; // remove loader

        if (user && this.getName() !== user?.nickname) {
            this.additionalInfoElement.appendChild(this.createRealNicknameElement(user));
        }

        this.additionalInfoElement.appendChild(this.createDurationElement(user));
    }

    private createDurationElement(user: UserShortModel | null): Element {
        const element = createFromHtml('<div class="pp--additional-user-info__item pp--additional-user-info__item_point"></div>');

        if (!user) {
            setHint(element, `Не удалось получить информацию по этому пользователю`);
            return element;
        }

        const regDate = user.registrationDate;

        const dayDate = sub(new Date(), { days: 1 });
        const monthDate = sub(new Date(), { months: 1 });
        const yearDate = sub(new Date(), { years: 1 });

        if (regDate > dayDate) {
            element.classList.add('pp--additional-user-info__item_danger');
            setHint(element, `Зарегистрирован менее суток`);
        } else if (regDate > monthDate) {
            element.classList.add('pp--additional-user-info__item_warning');
            setHint(element, `Зарегистрирован менее месяца`);
        } else if (regDate > yearDate) {
            element.classList.add('pp--additional-user-info__item_info');
            setHint(element, `Зарегистрирован менее года`);
        } else {
            element.classList.add('pp--additional-user-info__item_success');
            setHint(element, `Зарегистрирован более года`);
        }

        return element;
    }

    private revertDomChanges() {
        this.additionalInfoElement?.remove();
        delete this.additionalInfoElement;
    }

    private createRealNicknameElement(user: UserShortModel): Element {
        const element = createFromHtml('<div class="pp--additional-user-info__item pp--additional-user-info__item_real-nickname"></div>');

        element.textContent = `${user.nickname}`;
        setHint(element, `Ник пользователя до удаления`);

        return element;
    }
}
