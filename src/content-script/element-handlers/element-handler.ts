import { fromEvent } from 'rxjs';

export interface ElementHandlerConstructor<THandler extends ElementHandler> {
    selector: string;

    new (element: Element): THandler;
}

export abstract class ElementHandler {
    static readonly selector: string;

    constructor(public readonly element: Element) {}

    event(eventName: string) {
        return fromEvent(this.element, eventName);
    }
}
