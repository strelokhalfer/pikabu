export function createFromHtml(html: string): Element {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = html;

    const element = wrapper.firstElementChild;

    if (!element) {
        throw new Error('Не удалось создать элемент');
    }

    return element;
}
