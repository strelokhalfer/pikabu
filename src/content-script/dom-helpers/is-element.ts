export function isElement(val: any): val is Element {
    return val instanceof Element;
}
