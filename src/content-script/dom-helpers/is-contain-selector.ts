export function isContainSelector(element: Element, selector: string): boolean {
    return element.matches(selector) || !!element.querySelector(selector);
}
