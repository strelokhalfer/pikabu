export function setHint(element: Element, hint: string | null): void {
    element.classList.toggle('hint', !!hint);
    element.setAttribute('aria-label', hint ?? '');
}
