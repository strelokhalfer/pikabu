export enum IconEnum {
    EyeClose = 'icon--ui__eye-close',
    Preloader = 'icon--ui__preloader',
}
