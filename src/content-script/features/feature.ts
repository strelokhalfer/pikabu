import { Observable } from 'rxjs';

export interface Feature {
    activeWhileSubscribe(): Observable<any>;
}
