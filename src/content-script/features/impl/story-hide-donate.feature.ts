import { Observable, switchMap } from 'rxjs';
import { Service } from 'typedi';
import { Feature } from '../feature';
import { HandlersService } from '../../services/handlers.service';
import { PageElementHandler } from '../../element-handlers/impl/page/page.element-handler';

@Service()
export class StoryHideDonateFeature implements Feature {
    constructor(private readonly handlersService: HandlersService) {}

    activeWhileSubscribe(): Observable<any> {
        return this.handlersService.listenOne(PageElementHandler).pipe(switchMap((page) => page.hideDonates()));
    }
}
