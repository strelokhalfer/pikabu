import { catchError, EMPTY, Observable } from 'rxjs';
import { Service } from 'typedi';
import { Feature } from '../feature';
import { HandlersService } from '../../services/handlers.service';
import { StoryElementHandler } from '../../element-handlers/impl/story.element-handler';
import { ErrorService } from '../../services/error.service';
import { switchEvery } from '../../../common/rxjs-operators/switch-every/switch-every';

@Service()
export class ForeverIgnoreToolbarButtonFeature implements Feature {
    constructor(
        private readonly handlersService: HandlersService,
        private readonly errorService: ErrorService,
    ) {}

    activeWhileSubscribe(): Observable<any> {
        return this.handlersService.listenAll(StoryElementHandler).pipe(
            switchEvery((story) =>
                story.showIgnoreButton().pipe(
                    catchError((error) => {
                        this.errorService.log(error);
                        return EMPTY;
                    }),
                ),
            ),
        );
    }
}
