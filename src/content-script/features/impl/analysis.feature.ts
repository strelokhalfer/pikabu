import { Observable } from 'rxjs';
import { EventEnum } from '../../../common/enums/event.enum';
import { Service } from 'typedi';
import { EventsService } from '../../../common/services/events/events.service';
import { Feature } from '../feature';
import { HandlersService } from '../../services/handlers.service';
import { StoryElementHandler } from '../../element-handlers/impl/story.element-handler';
import { switchEvery } from '../../../common/rxjs-operators/switch-every/switch-every';

@Service()
export class AnalysisFeature implements Feature {
    constructor(
        private readonly eventsService: EventsService,
        private readonly handlersService: HandlersService,
    ) {
        void this.eventsService.emitEvent(EventEnum.SetUserAgent, navigator.userAgent);
    }

    activeWhileSubscribe(): Observable<any> {
        return this.activeShareActions();
    }

    protected activeShareActions(): Observable<any> {
        return this.handlersService.listenAll(StoryElementHandler).pipe(switchEvery((story) => story.listenRatingChange()));
    }
}
