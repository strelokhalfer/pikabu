import { Service } from 'typedi';
import { debounceTime, map, Observable } from 'rxjs';

@Service()
export class MutationsService {
    getAttributeMutations(element: Element): Observable<void> {
        return this.getTreeMutations(element, {
            attributes: true,
        }).pipe(
            map(() => void 0),
            debounceTime(0),
        );
    }

    getTreeMutations(element: Element, options: MutationObserverInit = { subtree: true, childList: true }): Observable<MutationRecord> {
        return new Observable((subscriber) => {
            const observer = new MutationObserver((mutations) => {
                for (const mutation of mutations) {
                    subscriber.next(mutation);
                }
            });

            observer.observe(element, options);

            return () => observer.disconnect();
        });
    }
}
