import { Service } from 'typedi';
import { debounceTime, filter, map, Observable, startWith } from 'rxjs';
import { MutationsService } from './mutations.service';
import { isElement } from '../dom-helpers/is-element';
import { isContainSelector } from '../dom-helpers/is-contain-selector';

@Service()
export class PageMutationsService {
    constructor(private readonly mutationsService: MutationsService) {}

    listenTreeChanged(selector: string, scope: Element = document.body): Observable<readonly Element[]> {
        return this.mutationsService.getTreeMutations(scope).pipe(
            map((mutations) => [...mutations.addedNodes, ...mutations.removedNodes]),
            filter((addedNodes) => addedNodes.filter(isElement).some((element) => isContainSelector(element, selector))),
            startWith(null),
            debounceTime(0),
            map(() => scope.querySelectorAll(selector)),
            map((nodeList) => [...nodeList]),
        );
    }
}
