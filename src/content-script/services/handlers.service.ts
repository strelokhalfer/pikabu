import { Service } from 'typedi';
import { PageMutationsService } from './page-mutations.service';
import { filter, map, Observable } from 'rxjs';
import { ElementHandler, ElementHandlerConstructor } from '../element-handlers/element-handler';
import { HandlersCacheService } from './handlers-cache.service';

@Service()
export class HandlersService {
    constructor(
        private readonly pageMutationsService: PageMutationsService,
        private readonly handlersCacheService: HandlersCacheService,
    ) {}

    listenOne<T extends ElementHandler>(handlerType: ElementHandlerConstructor<T>, scope?: Element): Observable<T> {
        return this.listenAll(handlerType, scope).pipe(
            map((items) => items[0]),
            filter(Boolean),
        );
    }

    listenAll<T extends ElementHandler>(handlerType: ElementHandlerConstructor<T>, scope?: Element): Observable<readonly T[]> {
        const cache = this.handlersCacheService.getCacheMap(handlerType);

        return this.pageMutationsService.listenTreeChanged(handlerType.selector, scope).pipe(
            map((elements) => {
                const handlers: T[] = [];

                for (const element of elements) {
                    if (!cache.has(element)) {
                        cache.set(element, new handlerType(element));
                    }

                    handlers.push(cache.get(element)!);
                }

                return handlers;
            }),
        );
    }
}
