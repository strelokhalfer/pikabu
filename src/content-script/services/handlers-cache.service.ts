import { Service } from 'typedi';
import { ElementHandler, ElementHandlerConstructor } from '../element-handlers/element-handler';

@Service()
export class HandlersCacheService {
    private cache = new Map<any, Map<Element, any>>();

    getCacheMap<T extends ElementHandler>(handlerType: ElementHandlerConstructor<T>): Map<Element, T> {
        if (!this.cache.has(handlerType)) {
            this.cache.set(handlerType, new Map());
        }

        return this.cache.get(handlerType)!;
    }
}
