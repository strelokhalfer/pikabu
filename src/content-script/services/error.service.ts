import { Service } from 'typedi';
import { EventsService } from '../../common/services/events/events.service';
import { EventEnum } from '../../common/enums/event.enum';
import { errorStringify } from '../../common/functions/error-stringify';

@Service()
export class ErrorService {
    constructor(private readonly eventsService: EventsService) {}

    log(error: any): void {
        void this.eventsService.emitEvent(EventEnum.Error, errorStringify(error));
    }

    try<T>(fn: () => T): T {
        try {
            return fn();
        } catch (e) {
            this.log(e);
            throw e;
        }
    }
}
