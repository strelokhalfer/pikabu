import 'reflect-metadata';
import { Container } from 'typedi';
import { RuntimeToken } from '../common/tokens/runtime.token';
import { RuntimeEnum } from '../common/enums/runtime.enum';
import { AnalysisFeature } from './features/impl/analysis.feature';
import { combineLatest, config, EMPTY, Observable, switchMap } from 'rxjs';
import { ErrorService } from './services/error.service';
import { SettingsClientService } from '../common/services/settings-client.service';
import { getSettingsFeatures } from '../common/consts/get-settings-features';

Container.set(RuntimeToken, RuntimeEnum.Content);

config.onUnhandledError = (err) => {
    Container.get(ErrorService).log(err);
    console.error(err);
};

const settingsClientService = Container.get(SettingsClientService);

Container.get(AnalysisFeature).activeWhileSubscribe().subscribe();

const featuresEnabled$$: Observable<void>[] = getSettingsFeatures().map((featureMap) =>
    settingsClientService
        .getKey$(featureMap.settingsKey)
        .pipe(switchMap((enabled) => (enabled ? featureMap.feature.activeWhileSubscribe() : EMPTY))),
);

combineLatest(featuresEnabled$$).subscribe();
