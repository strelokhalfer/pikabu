function getDynamicRules(): Promise<any[]> {
    if (global.browser) {
        return browser.declarativeNetRequest.getDynamicRules();
    }

    return new Promise<any[]>((resolve) => {
        chrome.declarativeNetRequest.getDynamicRules(resolve);
    });
}

(async () => {
    const context = global.browser ?? chrome;
    const previousRules = await getDynamicRules();
    const previousRuleIds = previousRules.map((rule) => rule.id);
    void context.declarativeNetRequest.updateDynamicRules({
        removeRuleIds: previousRuleIds,
        addRules: [
            {
                id: 1,
                condition: {
                    urlFilter: 'https://api.pikabu.ru/*',
                    resourceTypes: ['xmlhttprequest'],
                },
                action: {
                    type: 'modifyHeaders',
                    requestHeaders: [
                        {
                            header: 'User-Agent',
                            value: 'ru.pikabu.pikabu/1.4.14 (iPhone 11; iOS 16.6.0)',
                            operation: 'set',
                        },
                        {
                            header: 'Origin',
                            operation: 'remove',
                        },
                        {
                            header: 'Cookie',
                            operation: 'remove',
                        },
                    ],
                },
            },
        ],
    });
})();
