import { Service } from 'typedi';
import { PikabuUserApiService, PikabuUserShortDto } from '../../api';
import { bufferTime, filter, firstValueFrom, from, map, mergeMap, share, Subject, withLatestFrom } from 'rxjs';
import { SettingsBackgroundService } from '../settings-background.service';

@Service()
export class UserFromPekabuBackgroundService {
    private readonly service = PikabuUserApiService;

    readonly request$ = new Subject<number>();

    readonly batch$ = this.request$.pipe(
        bufferTime(10),
        filter((users) => !!users.length),
        map((users) => new Set(users)),
    );

    readonly response$ = this.batch$.pipe(
        withLatestFrom(this.settingsBackgroundService.get()),
        mergeMap(([usersSet, settings]) =>
            from(this.service.pikabuUserControllerGetUsersById([...usersSet], settings.uuid)).pipe(
                map((usersResponse) => {
                    const map = new Map<number, PikabuUserShortDto | null>(usersResponse.map((user) => [user.id, user]));

                    const undefinedUsers = [...usersSet].filter((nickname) => !map.has(nickname));
                    undefinedUsers.forEach((nickname) => map.set(nickname, null));

                    return map;
                }),
            ),
        ),
        share(),
    );

    constructor(private readonly settingsBackgroundService: SettingsBackgroundService) {}

    async getShortUser(id: number): Promise<PikabuUserShortDto> {
        this.request$.next(id);

        const user = await firstValueFrom(
            this.response$.pipe(
                filter((users) => users.has(id)),
                map((users) => users.get(id)),
            ),
        );

        if (!user) {
            throw new Error(`User "${id}" not found in pekabu`);
        }

        return user;
    }
}
