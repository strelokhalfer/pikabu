import { Service } from 'typedi';
import { AnalysisBackgroundService } from './analysis-background.service';

@Service()
export class AnalysisActionsBackgroundService {
    constructor(private readonly actionsBackgroundService: AnalysisBackgroundService) {}

    error(text: string): void {
        this.actionsBackgroundService.action({
            error: {
                text: String(text),
            },
        });
    }

    storyUpvote(storyId: number): void {
        this.actionsBackgroundService.action({
            storyVote: {
                storyId,
                value: 1,
            },
        });
    }

    storyDownvote(storyId: number): void {
        this.actionsBackgroundService.action({
            storyVote: {
                storyId,
                value: -1,
            },
        });
    }
}
