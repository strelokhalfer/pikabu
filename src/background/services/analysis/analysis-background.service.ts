import { Service } from 'typedi';
import { bufferTime, catchError, EMPTY, filter, from, map, mergeMap, Observable, retry, share, Subject, zip } from 'rxjs';
import { PluginAnalysisActionV1Dto, PluginAnalysisKnownV1Dto, PluginApiService } from '../../api';
import { SettingsBackgroundService } from '../settings-background.service';

@Service()
export class AnalysisBackgroundService {
    readonly context = global.browser ?? chrome;

    private manifest = this.context.runtime.getManifest();
    private pluginVersion = this.manifest.version;
    private userAgent: string | null = null;

    private action$ = new Subject<PluginAnalysisActionV1Dto>();
    private known$ = new Subject<PluginAnalysisKnownV1Dto>();

    private actions$ = this.action$.pipe(bufferTime(5000));
    private knowledge$ = this.known$.pipe(bufferTime(5000));

    readonly collect$: Observable<void> = zip([this.actions$, this.knowledge$]).pipe(
        filter(([actions, knowledge]) => !!actions.length || !!knowledge.length),
        mergeMap(([actions, knowledge]) =>
            from(this.sendUsageAnalysis(actions, knowledge)).pipe(
                retry({
                    count: 2,
                    delay: 10000,
                }),
                catchError(() => EMPTY),
            ),
        ),
        map(() => void 0),
        share(),
    );

    constructor(private readonly settingsBackgroundService: SettingsBackgroundService) {}

    setUserAgent(data: string | null) {
        this.userAgent = data;
    }

    action(data: PluginAnalysisActionV1Dto): void {
        this.action$.next({
            ...data,
        });
    }

    known(data: PluginAnalysisKnownV1Dto): void {
        this.known$.next({
            ...data,
        });
    }

    async sendUsageAnalysis(actions: PluginAnalysisActionV1Dto[], knowledge: PluginAnalysisKnownV1Dto[]): Promise<void> {
        const settings = await this.settingsBackgroundService.get();

        await PluginApiService.pluginAnalysisV1ControllerAdd({
            pluginVersion: this.pluginVersion,
            userAgent: this.userAgent,
            pluginSyncUuid: settings.uuid,
            actions,
            knowledge,
        });
    }
}
