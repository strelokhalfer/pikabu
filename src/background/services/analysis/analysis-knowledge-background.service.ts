import { Service } from 'typedi';
import { AnalysisBackgroundService } from './analysis-background.service';
import {
    PluginAnalysisKnownCommentRatingV1Dto,
    PluginAnalysisKnownCommentV1Dto,
    PluginAnalysisKnownStoryRatingV1Dto,
    PluginAnalysisKnownStoryV1Dto,
    PluginAnalysisKnownUserV1Dto,
} from '../../api';

@Service()
export class AnalysisKnowledgeBackgroundService {
    private readonly usersAlreadySend = new Set<number>();
    private readonly storiesAlreadySend = new Set<number>();
    private readonly commentsAlreadySend = new Set<number>();

    constructor(private readonly actionsBackgroundService: AnalysisBackgroundService) {}

    knownUser(user: PluginAnalysisKnownUserV1Dto): void {
        if (this.usersAlreadySend.has(user.id)) {
            return;
        }
        this.usersAlreadySend.add(user.id);

        this.actionsBackgroundService.known({
            user,
        });
    }

    knownStory(story: PluginAnalysisKnownStoryV1Dto): void {
        if (this.storiesAlreadySend.has(story.id)) {
            return;
        }
        this.storiesAlreadySend.add(story.id);

        this.actionsBackgroundService.known({
            story,
        });
    }

    knownStoryRating(storyRating: PluginAnalysisKnownStoryRatingV1Dto): void {
        this.actionsBackgroundService.known({
            storyRating,
        });
    }

    knownComment(comment: PluginAnalysisKnownCommentV1Dto): void {
        if (this.commentsAlreadySend.has(comment.id)) {
            return;
        }
        this.commentsAlreadySend.add(comment.id);

        this.actionsBackgroundService.known({
            comment,
        });
    }

    knownCommentRating(commentRating: PluginAnalysisKnownCommentRatingV1Dto): void {
        this.actionsBackgroundService.known({
            commentRating,
        });
    }
}
