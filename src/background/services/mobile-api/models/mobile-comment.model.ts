export interface MobileCommentModel {
    readonly comment_id: number;
    readonly comment_rating: number | null;
    readonly comment_pluses: number | null;
    readonly comment_minuses: number | null;
    readonly comment_platform: number;
    readonly user_id: number;
    readonly comment_time: number;
    readonly story_id: number;
    readonly parent_id: number;
    readonly comment_desc: {
        images: unknown[];
        text: string;
        videos: unknown[];
    };
}
