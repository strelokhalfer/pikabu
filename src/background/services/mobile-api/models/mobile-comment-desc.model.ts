export interface MobileCommentDescModel {
    readonly images: unknown[];
    readonly text: string;
    readonly videos: unknown[];
}
