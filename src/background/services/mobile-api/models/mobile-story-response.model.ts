import { MobileStoryModel } from './mobile-story.model';
import { MobileCommentModel } from './mobile-comment.model';

export interface MobileStoryResponseModel {
    readonly story: MobileStoryModel;
    readonly comments: readonly MobileCommentModel[];
}
