export interface MobileStoryModel {
    readonly story_pluses: number | null;
    readonly story_minuses: number | null;
    readonly story_digs: number | null;
    readonly user_id: number;
    readonly user_name: string;
    readonly tags: string[];
    readonly story_id: number;
    readonly story_time: number;
    readonly story_title: string;
    readonly story_data: { type: string; data: object }[];
}
