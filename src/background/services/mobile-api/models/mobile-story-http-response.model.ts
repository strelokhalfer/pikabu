import { MobileStoryResponseModel } from './mobile-story-response.model';

export interface MobileStoryHttpResponseModel {
    readonly response?: MobileStoryResponseModel;
    readonly error?: unknown;
}
