import { Service } from 'typedi';
import { MobileStoryHttpResponseModel } from './models/mobile-story-http-response.model';
import { MobileStoryResponseModel } from './models/mobile-story-response.model';
import { MobileCommentModel } from './models/mobile-comment.model';
import { AnalysisKnowledgeBackgroundService } from '../analysis/analysis-knowledge-background.service';

@Service()
export class MobileApiBackgroundService {
    constructor(private readonly analysisKnowledgeBackgroundService: AnalysisKnowledgeBackgroundService) {}

    private async getStoryFullDataPage(storyId: number, page: number): Promise<MobileStoryResponseModel> {
        const request = {
            story_id: storyId,
            hash: null,
            page: page,
        };

        return fetch(`https://api.pikabu.ru/v1/story.get?page=${page}`, {
            method: 'POST',
            body: JSON.stringify(request),
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then((response) => response.json())
            .then((response: MobileStoryHttpResponseModel) => {
                if (response.error) {
                    return Promise.reject(response.error);
                }

                return response.response!;
            });
    }

    async getStoryDataAllPages(storyId: number): Promise<MobileStoryResponseModel> {
        let response = {} as MobileStoryResponseModel;
        let comments: MobileCommentModel[] = [];
        let page = 1;

        do {
            const pageResponse = await this.getStoryFullDataPage(storyId, page);
            comments = [...(pageResponse?.comments ?? [])];

            response = {
                ...response,
                ...pageResponse,
                comments: [...(response?.comments ?? []), ...comments],
            };
            page++;
        } while (comments.length === 100);

        try {
            this.sendCommentsToPekabu(response);
        } catch (e) {}

        return response;
    }

    async getStoryData(storyId: number): Promise<MobileStoryResponseModel> {
        const response = await this.getStoryFullDataPage(storyId, 1);

        try {
            this.sendStoryToPekabu(response);
            this.sendCommentsToPekabu(response);
        } catch (e) {}

        return response;
    }

    private sendStoryToPekabu({ story }: MobileStoryResponseModel): void {
        this.analysisKnowledgeBackgroundService.knownStory({
            id: story.story_id,
            authorId: story.user_id,
            tags: story.tags,
            title: story.story_title,
            time: new Date(story.story_time * 1000).toISOString(),
            data: story.story_data.map((item) => ({
                type: item.type,
                json: JSON.stringify(item.data),
            })),
        });

        if (story.story_pluses !== null && story.story_minuses !== null) {
            this.analysisKnowledgeBackgroundService.knownStoryRating({
                storyId: story.story_id,
                pluses: story.story_pluses,
                minuses: story.story_minuses,
            });
        }
    }

    private sendCommentsToPekabu(response: MobileStoryResponseModel): void {
        for (const comment of response.comments) {
            try {
                this.analysisKnowledgeBackgroundService.knownComment({
                    id: comment.comment_id,
                    authorId: comment.user_id,
                    storyId: comment.story_id,
                    parentId: comment.parent_id,
                    time: new Date(comment.comment_time * 1000).toISOString(),
                    text: comment.comment_desc.text,
                    imagesJson: JSON.stringify(comment.comment_desc.images),
                    videosJson: JSON.stringify(comment.comment_desc.videos),
                });

                if (comment.comment_pluses !== null && comment.comment_minuses !== null) {
                    this.analysisKnowledgeBackgroundService.knownCommentRating({
                        commentId: comment.comment_id,
                        pluses: comment.comment_pluses,
                        minuses: comment.comment_minuses,
                    });
                }
            } catch (e) {}
        }
    }
}
