import { Service } from 'typedi';

@Service()
export class IgnoreListBackgroundService {
    async addForever(authorId: number): Promise<void> {
        const form = new FormData();

        form.set('authors', authorId + '');
        form.set('period', 'forever');
        form.set('action', 'add_rule');
        form.set('story_id', '0');
        form.set('keywords', '');
        form.set('tags', '');
        form.set('communities', '');

        await fetch('https://pikabu.ru/ajax/ignore_actions.php', {
            method: 'POST',
            body: form,
        });
    }
}
