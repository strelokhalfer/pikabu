import { Service } from 'typedi';
import { SettingsModel } from '../../common/models/settings.model';
import { v4 } from 'uuid';
import { EventsService } from '../../common/services/events/events.service';
import { EventEnum } from '../../common/enums/event.enum';
import { DefaultSettingsConst } from '../../common/consts/default-settings.const';

@Service()
export class SettingsBackgroundService {
    readonly storage = global.browser ? browser.storage.local : chrome.storage.sync;

    constructor(private readonly eventsService: EventsService) {}

    async get(): Promise<SettingsModel> {
        let settings = (await this.storage.get()) as SettingsModel;

        if (!settings || !settings.version || settings.version < DefaultSettingsConst.version) {
            settings = {
                ...DefaultSettingsConst,
                ...settings,
                version: DefaultSettingsConst.version,
                uuid: settings.uuid ?? v4(),
            };

            await this.set(settings);
        }

        return settings;
    }

    async set(settings: SettingsModel) {
        await this.storage.set(settings);
        await this.eventsService.emitEvent(EventEnum.SettingsSaved, settings);
    }
}
