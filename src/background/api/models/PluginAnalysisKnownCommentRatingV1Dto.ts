/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisKnownCommentRatingV1Dto = {
    commentId: number;
    pluses: number;
    minuses: number;
};
