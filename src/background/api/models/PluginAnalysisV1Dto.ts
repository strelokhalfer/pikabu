/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PluginAnalysisActionV1Dto } from './PluginAnalysisActionV1Dto';
import type { PluginAnalysisKnownV1Dto } from './PluginAnalysisKnownV1Dto';

export type PluginAnalysisV1Dto = {
    pluginSyncUuid: string;
    pluginVersion: string;
    userAgent?: string | null;
    actions: Array<PluginAnalysisActionV1Dto>;
    knowledge: Array<PluginAnalysisKnownV1Dto>;
};
