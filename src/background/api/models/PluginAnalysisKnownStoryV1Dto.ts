/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PluginAnalysisKnownStoryDataV1Dto } from './PluginAnalysisKnownStoryDataV1Dto';

export type PluginAnalysisKnownStoryV1Dto = {
    id: number;
    time: string;
    title?: string;
    authorId: number;
    tags: Array<string>;
    data: Array<PluginAnalysisKnownStoryDataV1Dto>;
};
