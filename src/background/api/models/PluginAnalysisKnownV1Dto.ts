/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PluginAnalysisKnownCommentRatingV1Dto } from './PluginAnalysisKnownCommentRatingV1Dto';
import type { PluginAnalysisKnownCommentV1Dto } from './PluginAnalysisKnownCommentV1Dto';
import type { PluginAnalysisKnownStoryRatingV1Dto } from './PluginAnalysisKnownStoryRatingV1Dto';
import type { PluginAnalysisKnownStoryV1Dto } from './PluginAnalysisKnownStoryV1Dto';
import type { PluginAnalysisKnownUserV1Dto } from './PluginAnalysisKnownUserV1Dto';

export type PluginAnalysisKnownV1Dto = {
    user?: PluginAnalysisKnownUserV1Dto;
    story?: PluginAnalysisKnownStoryV1Dto;
    storyRating?: PluginAnalysisKnownStoryRatingV1Dto;
    comment?: PluginAnalysisKnownCommentV1Dto;
    commentRating?: PluginAnalysisKnownCommentRatingV1Dto;
};
