/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisUserV0Dto = {
    nickname: string | null;
    pluginSyncUuid: string;
    backgroundUuid: string;
};
