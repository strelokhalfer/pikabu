/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PluginAnalysisActionV0Dto } from './PluginAnalysisActionV0Dto';

export type PluginAnalysisV0Dto = {
    pluginVersion: string;
    userAgent?: string | null;
    arch: string;
    os: string;
    actions: Array<PluginAnalysisActionV0Dto>;
};
