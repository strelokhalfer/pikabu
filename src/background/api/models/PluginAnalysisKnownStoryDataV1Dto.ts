/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisKnownStoryDataV1Dto = {
    type: string;
    json: string;
};
