/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PluginAnalysisActionAuthorIgnoreV1Dto } from './PluginAnalysisActionAuthorIgnoreV1Dto';
import type { PluginAnalysisActionCommentVoteV1Dto } from './PluginAnalysisActionCommentVoteV1Dto';
import type { PluginAnalysisActionErrorV1Dto } from './PluginAnalysisActionErrorV1Dto';
import type { PluginAnalysisActionStoryVoteV1Dto } from './PluginAnalysisActionStoryVoteV1Dto';

export type PluginAnalysisActionV1Dto = {
    storyVote?: PluginAnalysisActionStoryVoteV1Dto;
    commentVote?: PluginAnalysisActionCommentVoteV1Dto;
    ignore?: PluginAnalysisActionAuthorIgnoreV1Dto;
    error?: PluginAnalysisActionErrorV1Dto;
};
