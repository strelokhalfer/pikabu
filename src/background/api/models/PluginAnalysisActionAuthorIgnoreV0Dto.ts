/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisActionAuthorIgnoreV0Dto = {
    storyId: number;
    authorId: number;
};
