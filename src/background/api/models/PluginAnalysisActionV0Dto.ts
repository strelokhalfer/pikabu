/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PluginAnalysisActionAuthorIgnoreV0Dto } from './PluginAnalysisActionAuthorIgnoreV0Dto';
import type { PluginAnalysisActionCommentVoteV0Dto } from './PluginAnalysisActionCommentVoteV0Dto';
import type { PluginAnalysisActionErrorV0Dto } from './PluginAnalysisActionErrorV0Dto';
import type { PluginAnalysisActionOpenPageV0Dto } from './PluginAnalysisActionOpenPageV0Dto';
import type { PluginAnalysisActionStoryVoteV0Dto } from './PluginAnalysisActionStoryVoteV0Dto';
import type { PluginAnalysisUserV0Dto } from './PluginAnalysisUserV0Dto';

export type PluginAnalysisActionV0Dto = {
    user: PluginAnalysisUserV0Dto;
    uuid: string;
    time: string;
    storyVote?: PluginAnalysisActionStoryVoteV0Dto;
    commentVote?: PluginAnalysisActionCommentVoteV0Dto;
    ignore?: PluginAnalysisActionAuthorIgnoreV0Dto;
    openPage?: PluginAnalysisActionOpenPageV0Dto;
    error?: PluginAnalysisActionErrorV0Dto;
};
