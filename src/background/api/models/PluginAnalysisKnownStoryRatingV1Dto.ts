/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisKnownStoryRatingV1Dto = {
    storyId: number;
    pluses: number;
    minuses: number;
};
