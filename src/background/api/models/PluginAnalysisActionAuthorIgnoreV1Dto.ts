/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisActionAuthorIgnoreV1Dto = {
    storyId: number;
    authorId: number;
};
