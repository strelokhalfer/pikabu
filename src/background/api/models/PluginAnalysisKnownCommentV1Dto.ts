/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PluginAnalysisKnownCommentV1Dto = {
    id: number;
    parentId: number;
    time: string;
    storyId: number;
    authorId: number;
    text: string;
    imagesJson: string;
    videosJson: string;
};
