/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PikabuUserShortDto } from '../models/PikabuUserShortDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class PikabuUserApiService {

    /**
     * @param requestBody 
     * @param xPluginUuid 
     * @returns PikabuUserShortDto 
     * @throws ApiError
     */
    public static pikabuUserControllerGetUsersByNickname(
requestBody: Array<string>,
xPluginUuid?: string,
): CancelablePromise<Array<PikabuUserShortDto>> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/pikabu-user/find-many-by-nickname',
            headers: {
                'x-plugin-uuid': xPluginUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody 
     * @param xPluginUuid 
     * @returns PikabuUserShortDto 
     * @throws ApiError
     */
    public static pikabuUserControllerGetUsersById(
requestBody: Array<number>,
xPluginUuid?: string,
): CancelablePromise<Array<PikabuUserShortDto>> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/pikabu-user/find-many-by-id',
            headers: {
                'x-plugin-uuid': xPluginUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
