/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PluginAnalysisV0Dto } from '../models/PluginAnalysisV0Dto';
import type { PluginAnalysisV1Dto } from '../models/PluginAnalysisV1Dto';
import type { PluginSsEventDto } from '../models/PluginSsEventDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class PluginApiService {

    /**
     * @param requestBody 
     * @returns any 
     * @throws ApiError
     */
    public static pluginAnalysisV0ControllerAdd(
requestBody: PluginAnalysisV0Dto,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/plugin/analysis',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody 
     * @returns any 
     * @throws ApiError
     */
    public static pluginAnalysisV1ControllerAdd(
requestBody: PluginAnalysisV1Dto,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/plugin/v1/analysis',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param uuid 
     * @returns PluginSsEventDto 
     * @throws ApiError
     */
    public static pluginSseControllerSse(
uuid: string,
): CancelablePromise<PluginSsEventDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/plugin/sse/{uuid}',
            path: {
                'uuid': uuid,
            },
        });
    }

}
