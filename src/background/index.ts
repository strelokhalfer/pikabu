import 'reflect-metadata';
import './set-request-rules';
// Your other imports and initialization code
// comes here after you imported the reflect-metadata package!
import { Container } from 'typedi';
import { EventsService } from '../common/services/events/events.service';
import { RuntimeToken } from '../common/tokens/runtime.token';
import { RuntimeEnum } from '../common/enums/runtime.enum';
import { CommandService } from '../common/services/command/command.service';
import { CommandEnum } from '../common/enums/command.enum';
import { IgnoreListBackgroundService } from './services/ignore-list-background.service';
import { SettingsBackgroundService } from './services/settings-background.service';
import { MobileApiBackgroundService } from './services/mobile-api/mobile-api-background.service';
import { EventEnum } from '../common/enums/event.enum';
import { AnalysisBackgroundService } from './services/analysis/analysis-background.service';
import { OpenAPI, PluginAnalysisKnownUserV1Dto } from './api';
import { AnalysisActionsBackgroundService } from './services/analysis/analysis-actions-background.service';
import { AnalysisKnowledgeBackgroundService } from './services/analysis/analysis-knowledge-background.service';
import { UserFromPekabuBackgroundService } from './services/user/user-from-pekabu-background.service';

// OpenAPI.BASE = 'http://localhost:6180';
OpenAPI.BASE = 'https://pekabu.org';

Container.set(RuntimeToken, RuntimeEnum.Background);

const eventsService = Container.get(EventsService);
const commandService = Container.get(CommandService);
const ignoreListService = Container.get(IgnoreListBackgroundService);
const settingsService = Container.get(SettingsBackgroundService);
const storyService = Container.get(MobileApiBackgroundService);
const analysisService = Container.get(AnalysisBackgroundService);
const analysisActionsService = Container.get(AnalysisActionsBackgroundService);
const analysisKnowledgeBackgroundService = Container.get(AnalysisKnowledgeBackgroundService);
const userFromPekabuBackgroundService = Container.get(UserFromPekabuBackgroundService);

commandService.setProcessor(CommandEnum.IgnoreForever, (authorId) => ignoreListService.addForever(authorId)).subscribe();

commandService.setProcessor(CommandEnum.GetSettings, () => settingsService.get()).subscribe();
commandService.setProcessor(CommandEnum.SetSettings, (settings) => settingsService.set(settings)).subscribe();
commandService.setProcessor(CommandEnum.GetStoryData, (storyId) => storyService.getStoryData(storyId)).subscribe();
commandService.setProcessor(CommandEnum.GetStoryDataAllComments, (storyId) => storyService.getStoryDataAllPages(storyId)).subscribe();
commandService.setProcessor(CommandEnum.GetUserShortDataFromPekabu, (id) => userFromPekabuBackgroundService.getShortUser(id)).subscribe();

eventsService.listenEvent<string | null>(EventEnum.SetUserAgent).subscribe((event) => analysisService.setUserAgent(event.data));

eventsService.listenEvent<number>(EventEnum.StoryUpvote).subscribe((event) => analysisActionsService.storyUpvote(event.data));
eventsService.listenEvent<number>(EventEnum.StoryDownvote).subscribe((event) => analysisActionsService.storyDownvote(event.data));
eventsService.listenEvent<string>(EventEnum.Error).subscribe((event) => analysisActionsService.error(event.data));

eventsService
    .listenEvent<PluginAnalysisKnownUserV1Dto>(EventEnum.KnownUser)
    .subscribe((event) => analysisKnowledgeBackgroundService.knownUser(event.data));

analysisService.collect$.subscribe();
