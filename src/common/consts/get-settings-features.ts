import { FeatureMapModel } from '../models/feature-map.model';
import { Container } from 'typedi';
import { ForeverIgnoreToolbarButtonFeature } from '../../content-script/features/impl/forever-ignore-toolbar-button.feature';
import { StoryRatingFeature } from '../../content-script/features/impl/story-rating.feature';
import { StoryCommentRatingFeature } from '../../content-script/features/impl/story-comment-rating.feature';
import { StoryHideShortDesignFeature } from '../../content-script/features/impl/story-hide-short-design.feature';
import { StoryHideDonateFeature } from '../../content-script/features/impl/story-hide-donate.feature';
import { StoryUserRegistrationDateIndicatorFeature } from '../../content-script/features/impl/story-user-registration-date-indicator.feature';

export function getSettingsFeatures(): FeatureMapModel[] {
    return [
        {
            settingsKey: 'addForeverIgnore',
            feature: Container.get(ForeverIgnoreToolbarButtonFeature),
        },
        {
            settingsKey: 'showStoryVotes',
            feature: Container.get(StoryRatingFeature),
        },
        {
            settingsKey: 'showCommentVotes',
            feature: Container.get(StoryCommentRatingFeature),
        },
        {
            settingsKey: 'showUserRegistrationDateIndicator',
            feature: Container.get(StoryUserRegistrationDateIndicatorFeature),
        },
        {
            settingsKey: 'hideShortStoryDesign',
            feature: Container.get(StoryHideShortDesignFeature),
        },
        {
            settingsKey: 'hideDonateButton',
            feature: Container.get(StoryHideDonateFeature),
        },
    ];
}
