import { SettingsItemGroupModel } from '../../popup/components/settings/settings-item-group.model';

export const PopupSettingsConst: SettingsItemGroupModel[] = [
    {
        title: 'Игнор-лист',
        items: [
            {
                title: 'Добавить кнопку быстрого игнора',
                property: 'addForeverIgnore',
            },
        ],
    },
    {
        title: 'Рейтинг',
        items: [
            {
                title: 'Отображать рейтинг постов',
                property: 'showStoryVotes',
            },
            {
                title: 'Отображать рейтинг комментариев',
                property: 'showCommentVotes',
            },
        ],
    },
    {
        title: 'Пользователи',
        items: [
            {
                title: 'Отображать индикатор срока регистрации пользователя',
                property: 'showUserRegistrationDateIndicator',
            },
        ],
    },
    {
        title: 'Косметические изменения',
        items: [
            {
                title: 'Убрать дизайн коротких постов',
                property: 'hideShortStoryDesign',
            },
            {
                title: 'Скрыть кнопку доната',
                property: 'hideDonateButton',
            },
        ],
    },
];
