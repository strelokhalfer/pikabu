import { SettingsModel } from '../models/settings.model';

export const DefaultSettingsConst: Omit<SettingsModel, 'uuid'> = {
    version: 5,
    addForeverIgnore: true,
    showStoryVotes: true,
    showUserRegistrationDateIndicator: true,
    showCommentVotes: true,
    hideShortStoryDesign: false,
    hideDonateButton: false,
};
