import { Service } from 'typedi';
import { CommandService } from '../command/command.service';
import { UserShortModel } from './user-short.model';
import { CommandEnum } from '../../enums/command.enum';
import { createFromHtml } from '../../../content-script/dom-helpers/create-from-html';
import { Duration, sub } from 'date-fns';
import { EventsService } from '../events/events.service';
import { EventEnum } from '../../enums/event.enum';
import { PikabuUserShortDto, PluginAnalysisKnownUserV1Dto } from '../../../background/api';

@Service()
export class UserClientService {
    constructor(
        private readonly commandService: CommandService,
        private readonly eventsService: EventsService,
    ) {}

    async getShortUser(id: number | null, nickname: string | null): Promise<UserShortModel | null> {
        if (!id) {
            return null;
        }

        try {
            return await this.loadFromPekabuOrFail(id);
        } catch (e) {}

        if (!nickname) {
            return null;
        }

        return await this.loadFromPikabuOrFail(nickname);
    }

    private async loadFromPekabuOrFail(id: number): Promise<UserShortModel> {
        const dto = await this.commandService.execute<number, PikabuUserShortDto>(CommandEnum.GetUserShortDataFromPekabu, id, 2000);
        return {
            ...dto,
            registrationDate: new Date(dto.registrationDate),
        };
    }

    private async loadFromPikabuOrFail(nickname: string): Promise<UserShortModel> {
        const html = await this.getUserShortDataHtml(nickname);
        const element = createFromHtml(html);

        const idStr = element.getAttribute('data-user-id');
        const id = Number(idStr);

        const durationElement = element.querySelector('.profile__user-information span');
        const durationStr = durationElement?.textContent ?? '';
        const regDuration = this.parseDuration(durationStr);

        const user: UserShortModel = {
            id,
            nickname,
            registrationDate: sub(new Date(), regDuration),
        };

        void this.eventsService.emitEvent<PluginAnalysisKnownUserV1Dto>(EventEnum.KnownUser, {
            ...user,
            registrationDate: user.registrationDate.toISOString(),
        });

        return user;
    }

    private parseDuration(durationStr: string): Duration {
        const duration: Duration = {};
        const units: { [key in keyof Duration]: string[] } = {
            years: ['год', 'лет'],
            months: ['мес'],
            weeks: ['нед'],
            days: ['д', 'сут'],
            hours: ['час'],
        };
        const tokens = durationStr.split(' ');

        for (let i = 0; i < tokens.length; i += 2) {
            const value = tokens[i] ?? '';
            const unitFull = tokens[i + 1] ?? '';

            for (const unitKey in units) {
                const k = unitKey as keyof Duration;
                const unitSubstrings = units[k]!;

                if (unitSubstrings.some((start) => unitFull.startsWith(start))) {
                    duration[k] = Number(value);
                }
            }
        }

        return duration;
    }

    private async getUserShortDataHtml(nickname: string): Promise<string> {
        const fd = new FormData();

        fd.set('action', 'get_short_profile');
        fd.set('user_name', nickname);

        const result = await fetch('https://pikabu.ru/ajax/user_info.php', {
            method: 'post',
            body: fd,
        });

        const json = await result.json();

        if (!json || !json.data || !json.data.html) {
            throw new Error(`Неожиданный ответ`);
        }

        return json.data.html;
    }
}
