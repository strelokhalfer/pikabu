export interface UserShortModel {
    readonly id: number;
    readonly nickname: string;
    readonly registrationDate: Date;
}
