import { Service } from 'typedi';
import { SettingsModel } from '../models/settings.model';
import { EventsService } from './events/events.service';
import { EventEnum } from '../enums/event.enum';
import { CommandService } from './command/command.service';
import { map, merge, Observable } from 'rxjs';
import { CommandEnum } from '../enums/command.enum';
import { selectKey } from '../rxjs-operators/select-key/select-key';

@Service()
export class SettingsClientService {
    constructor(
        private readonly eventsService: EventsService,
        private readonly commandService: CommandService,
    ) {}

    get$(): Observable<SettingsModel> {
        return merge(this.get(), this.listenSave$());
    }

    getKey$<K extends keyof SettingsModel>(key: K): Observable<SettingsModel[K]> {
        return merge(this.get(), this.listenSave$()).pipe(selectKey(key));
    }

    async setKey<K extends keyof SettingsModel>(key: K, value: SettingsModel[K]): Promise<void> {
        let settings = await this.get();
        settings = {
            ...settings,
            [key]: value,
        };

        await this.set(settings);
    }

    async get(): Promise<SettingsModel> {
        return this.commandService.execute(CommandEnum.GetSettings, null);
    }

    async set(settings: SettingsModel): Promise<void> {
        return this.commandService.execute(CommandEnum.SetSettings, settings);
    }

    private listenSave$(): Observable<SettingsModel> {
        return this.eventsService.listenEvent(EventEnum.SettingsSaved).pipe(map((event) => event.data as SettingsModel));
    }
}
