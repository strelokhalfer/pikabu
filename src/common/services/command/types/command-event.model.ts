export interface CommandEventModel<TPayload> {
    readonly commandUuid: string;
    readonly commandName: string;
    readonly payload: TPayload;
    readonly error: any | null;
}
