import { Service } from 'typedi';
import { EventsService } from '../events/events.service';
import { delay, filter, firstValueFrom, Observable, switchMap, take, tap, timeout } from 'rxjs';
import { CommandProcessorType } from './types/command-processor.type';
import { CommandEventModel } from './types/command-event.model';
import { v4 as uuid } from 'uuid';
import { EventEnum } from '../../enums/event.enum';
import { CommandEnum } from '../../enums/command.enum';
import { errorStringify } from '../../functions/error-stringify';
import { EventModel } from '../events/models/event.model';

@Service()
export class CommandService {
    private readonly request$ = this.communicationService.listenEvent<CommandEventModel<any>>(EventEnum.CommandRequest);
    private readonly progress$ = this.communicationService.listenEvent<CommandEventModel<any>>(EventEnum.CommandProgress);
    private readonly complete$ = this.communicationService.listenEvent<CommandEventModel<any>>(EventEnum.CommandComplete);

    constructor(private readonly communicationService: EventsService) {}

    setProcessor(commandName: CommandEnum, processor: CommandProcessorType): Observable<void> {
        return this.request$.pipe(
            filter((event) => event.data.commandName === commandName),
            delay(Math.round(Math.random() * 50 + 20)),
            tap((event) =>
                this.communicationService.emitEvent(EventEnum.CommandProgress, {
                    ...event.data,
                    payload: null,
                }),
            ),
            delay(Math.round(Math.random() * 50 + 20)),
            switchMap(async (event) => {
                try {
                    const result = await processor(event.data.payload);

                    await this.communicationService.emitEvent(EventEnum.CommandComplete, {
                        ...event.data,
                        payload: result,
                        error: null,
                    });
                } catch (e) {
                    await this.communicationService.emitEvent(EventEnum.CommandComplete, {
                        ...event.data,
                        payload: null,
                        error: errorStringify(e),
                    });
                }
            }),
        );
    }

    async execute<TRequest, TResponse>(commandName: CommandEnum, data: TRequest, timeoutMs = 30000): Promise<TResponse> {
        const event: CommandEventModel<TRequest> = {
            commandName,
            commandUuid: uuid(),
            payload: data,
            error: null,
        };

        const progress$ = this.progress$.pipe(
            filter((e) => e.data.commandUuid === event.commandUuid),
            take(1),
        );

        const complete$ = this.complete$.pipe(
            filter((e) => e.data.commandUuid === event.commandUuid),
            take(1),
        );

        await this.communicationService.emitEvent(EventEnum.CommandRequest, event);

        try {
            await firstValueFrom(progress$.pipe(timeout(1000)));
        } catch (e) {
            throw new Error(`Время ожидания подтверждения вышло`);
        }

        let result: EventModel<CommandEventModel<any>>;

        try {
            result = await firstValueFrom(complete$.pipe(timeout(timeoutMs)));
        } catch (e) {
            throw new Error(`Время ожидания выполнения запроса вышло`);
        }

        if (result.data.error !== null) {
            throw new Error(result.data.error);
        }

        return result.data.payload;
    }
}
