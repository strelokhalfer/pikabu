import { RuntimeEnum } from '../../../enums/runtime.enum';

export interface EventModel<TPayload> {
    readonly type: string;
    readonly data: TPayload;
    readonly source: RuntimeEnum;
}
