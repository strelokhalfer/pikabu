import { filter, Observable, share, tap } from 'rxjs';
import { EventModel } from './models/event.model';
import { RuntimeEnum } from '../../enums/runtime.enum';
import { Inject, Service } from 'typedi';
import { RuntimeToken } from '../../tokens/runtime.token';
import { EventEnum } from '../../enums/event.enum';

@Service()
export class EventsService {
    readonly context = global.browser ?? chrome;

    private readonly events$ = this.listenAllEvents().pipe(
        // tap(event => console.log(event)),
        share(),
    );

    constructor(@Inject(RuntimeToken) private readonly runtime: RuntimeEnum) {}

    listenEvent<TPayload>(eventType: EventEnum): Observable<EventModel<TPayload>> {
        return this.events$.pipe(filter((eventObj) => eventObj.type === eventType));
    }

    async emitEvent<TPayload>(eventType: EventEnum, data: TPayload): Promise<void> {
        const eventObj: EventModel<TPayload> = {
            type: eventType,
            data,
            source: this.runtime,
        };

        if (this.runtime !== RuntimeEnum.Content) {
            const tabs = await this.context.tabs.query({});
            for (const tab of tabs) {
                if (!tab.id) {
                    continue;
                }

                try {
                    await this.context.tabs.sendMessage(tab.id, eventObj);
                } catch (e) {}
            }
        }

        try {
            await this.context.runtime.sendMessage(eventObj);
        } catch (e) {}
    }

    private listenAllEvents(): Observable<EventModel<any>> {
        return new Observable<EventModel<any>>((subscriber) => {
            const listener = (message: EventModel<any>) => {
                subscriber.next(message);
            };

            this.context.runtime.onMessage.addListener(listener);

            return () => this.context.runtime.onMessage.removeListener(listener);
        });
    }
}
