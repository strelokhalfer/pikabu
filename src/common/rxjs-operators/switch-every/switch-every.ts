import {
    combineLatest,
    debounceTime,
    from,
    Observable,
    ObservableInput,
    ObservedValueOf,
    of,
    OperatorFunction,
    share,
    shareReplay,
    startWith,
    switchMap,
    timer,
} from 'rxjs';

export function switchEvery<TIn, O extends ObservableInput<any>>(
    project: (value: TIn) => O,
): OperatorFunction<readonly TIn[], (ObservedValueOf<O> | null)[]> {
    const inputSharedObservablesMap = new Map<TIn, Observable<ObservedValueOf<O> | null>>();

    return switchMap((inputItems) => {
        const newInputItemsSet = new Set(inputItems);

        const toDeleteItems = [...inputSharedObservablesMap.keys()].filter((item) => !newInputItemsSet.has(item));

        toDeleteItems.forEach((toDeleteItem) => {
            inputSharedObservablesMap.delete(toDeleteItem);
        });

        const toAddItems = inputItems.filter((item) => !inputSharedObservablesMap.has(item));

        toAddItems.forEach((toAddItem) => {
            const observable = from(project(toAddItem));

            const sharedObservable = observable.pipe(
                startWith(null),
                debounceTime(0),
                shareReplay({
                    refCount: true,
                    bufferSize: 1,
                }),
            );

            inputSharedObservablesMap.set(toAddItem, sharedObservable);
        });

        if (!inputItems.length) {
            return of([]); // ?
        }

        const observables = inputItems.map((item) => inputSharedObservablesMap.get(item)!);

        return combineLatest(observables).pipe(
            share({
                resetOnRefCountZero: () => timer(1),
            }),
        );
    });
}
