import { switchEvery } from './switch-every';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';

describe(switchEvery.name, () => {
    it('Должен вернуть значения из обоих потоков', async () => {
        jest.useFakeTimers();

        const result = jest.fn();

        const obs1$ = new ReplaySubject(1);
        const obs2$ = new ReplaySubject(1);

        const common$ = new Subject<Observable<any>[]>();

        common$.pipe(switchEvery((obs) => obs)).subscribe(result);

        expect(result).toBeCalledTimes(0);

        common$.next([]);

        await jest.runAllTimersAsync();

        expect(result).toBeCalledTimes(1);
        expect(result).toHaveBeenLastCalledWith([]);

        common$.next([obs1$]);
        obs1$.next(1);

        await jest.runAllTimersAsync();

        expect(result).toBeCalledTimes(2);
        expect(result).toHaveBeenLastCalledWith([1]);

        common$.next([obs1$, obs2$]);
        obs2$.next(2);

        await jest.runAllTimersAsync();

        expect(result).toBeCalledTimes(3);
        expect(result).toHaveBeenLastCalledWith([1, 2]);
    });
});
