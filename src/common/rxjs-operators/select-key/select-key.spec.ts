import { selectKey } from './select-key';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

describe(selectKey.name, () => {
    type Obj = { a: string; b: string };

    it('Должны отфильтроваться только изменения по ключу объекта', () => {
        const originCallback = jest.fn();
        const filteredCallback = jest.fn();
        const origin = new Subject<Obj>();

        const sub = origin.pipe(tap(originCallback), selectKey('a')).subscribe(filteredCallback);

        origin.next({
            a: '100',
            b: '200',
        });

        expect(originCallback).toBeCalledTimes(1);
        expect(filteredCallback).toBeCalledTimes(1);

        origin.next({
            a: '100',
            b: '201',
        });

        expect(originCallback).toBeCalledTimes(2);
        expect(filteredCallback).toBeCalledTimes(1);

        origin.next({
            a: '101',
            b: '201',
        });

        expect(originCallback).toBeCalledTimes(3);
        expect(filteredCallback).toBeCalledTimes(2);

        sub.unsubscribe();
    });
});
