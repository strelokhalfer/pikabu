import { distinctUntilKeyChanged, OperatorFunction, pipe } from 'rxjs';
import { map } from 'rxjs/operators';

export function selectKey<TIn extends Object, TKey extends keyof TIn>(key: TKey): OperatorFunction<TIn, TIn[TKey]> {
  return pipe(
    distinctUntilKeyChanged(key),
    map((value) => value[key]),
  );
}
