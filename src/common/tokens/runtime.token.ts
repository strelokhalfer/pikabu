import { Token } from 'typedi';
import { RuntimeEnum } from '../enums/runtime.enum';

export const RuntimeToken = new Token<RuntimeEnum>('EventSource');
