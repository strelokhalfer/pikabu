export interface SettingsModel {
    version: number;
    uuid: string;
    addForeverIgnore: boolean;
    showStoryVotes: boolean;
    showCommentVotes: boolean;
    showUserRegistrationDateIndicator: boolean;
    hideShortStoryDesign: boolean;
    hideDonateButton: boolean;
}
