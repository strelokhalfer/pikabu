import { SettingsModel } from './settings.model';
import { Feature } from '../../content-script/features/feature';

export interface FeatureMapModel {
    settingsKey: keyof SettingsModel;
    feature: Feature;
}
