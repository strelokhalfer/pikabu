export function errorStringify(error: any): string {
    let errorStr = '';

    if (typeof error === 'string') {
        errorStr = error;
    } else if (error instanceof Error) {
        errorStr = error.toString();
    } else {
        errorStr = JSON.stringify(error);
    }

    return errorStr;
}
