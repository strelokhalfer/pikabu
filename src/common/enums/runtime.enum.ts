export enum RuntimeEnum {
    Background = 'background',
    Popup = 'popup',
    Content = 'content',
}
