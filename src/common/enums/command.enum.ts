export enum CommandEnum {
    IgnoreForever = 'ignore-forever',
    GetSettings = 'get-settings',
    SetSettings = 'set-settings',
    GetStoryData = 'get-story-data',
    GetStoryDataAllComments = 'get-story-data-all-comments',
    GetUserShortDataFromPekabu = 'get-user-short-data-from-pekabu',
}
