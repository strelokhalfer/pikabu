export enum EventEnum {
    CommandRequest = 'command-request',
    CommandProgress = 'command-progress',
    CommandComplete = 'command-complete',

    SettingsSaved = 'settings-saved',

    SetUserAgent = 'set-user-agent',
    StoryUpvote = 'story-upvote',
    StoryDownvote = 'story-downvote',

    Error = 'error',

    KnownUser = 'known-user',
}
