import 'reflect-metadata';
import { Container } from 'typedi';
import { RuntimeToken } from '../common/tokens/runtime.token';
import { RuntimeEnum } from '../common/enums/runtime.enum';
import { createRoot } from 'react-dom/client';
import { AppComponent } from './components/app/app.component';

Container.set(RuntimeToken, RuntimeEnum.Popup);

const rootElement = document.getElementById('root');
const root = createRoot(rootElement!);

root.render(<AppComponent />);
