import { SettingsComponent } from '../settings/settings.component';

import './app.component.scss';
import { Container } from 'typedi';
import { SettingsClientService } from '../../../common/services/settings-client.service';
import { useEffect, useState } from 'react';

export function AppComponent() {
    const [uuid, setUuid] = useState('');
    const settingsService = Container.get(SettingsClientService);

    useEffect(() => {
        const sub = settingsService.getKey$('uuid').subscribe(setUuid);
        return () => sub.unsubscribe();
    });

    return (
        <div>
            <SettingsComponent />
            <div className={'uuid'}>
                UUID: <span>{uuid}</span>
            </div>
        </div>
    );
}
