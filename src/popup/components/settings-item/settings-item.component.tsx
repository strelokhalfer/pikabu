import { SettingsItemProps } from './settings-item.props';
import { useState } from 'react';

import './settings-item.component.scss';

export function SettingsItemComponent(props: SettingsItemProps) {
    const [checked, setChecked] = useState(props.checked);

    const onChanged = (value: boolean) => {
        setChecked(value);
        props.checkedChange(value);
    };

    return (
        <label className={'settings-item'}>
            <input
                type={'checkbox'}
                checked={!!checked}
                onChange={(event) => onChanged(event.target.checked)}
            />
            <span>{props.title}</span>
        </label>
    );
}
