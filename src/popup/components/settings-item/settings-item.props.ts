export interface SettingsItemProps {
    readonly title: string;
    readonly checked: boolean;
    readonly checkedChange: (checked: boolean) => void;
}
