import { ReactNode } from 'react';

import './settings-items-group.scss';

type SettingsItemsGroupProps = {
    title: string;
    children?: ReactNode;
};

export function SettingsItemsGroup(props: SettingsItemsGroupProps) {
    return (
        <div className={'settings-items-group'}>
            <div className={'settings-items-group__title'}>{props.title}</div>
            <div className={'settings-items-group__content'}>{props.children}</div>
        </div>
    );
}
