import { Container } from 'typedi';
import { SettingsClientService } from '../../../common/services/settings-client.service';
import { useEffect, useState } from 'react';
import { SettingsModel } from '../../../common/models/settings.model';
import { SettingsItemComponent } from '../settings-item/settings-item.component';
import { SettingsItemsGroup } from '../settings-items-group/settings-items-group';

import './settings.component.scss';
import { PopupSettingsConst } from '../../../common/consts/popup-settings.const';
import { catchError, EMPTY } from 'rxjs';

export function SettingsComponent() {
    const [settings, setSettings] = useState<SettingsModel>();
    const [error, setError] = useState<string | null>('');
    const settingsService = Container.get(SettingsClientService);

    useEffect(() => {
        setError(null);
        const sub = settingsService
            .get$()
            .pipe(
                catchError((err) => {
                    setError(`Не удалось получить настройки от ServiceWorker\`а: ${err}`);
                    return EMPTY;
                }),
            )
            .subscribe(setSettings);
        return () => sub.unsubscribe();
    });

    const updateItem = (property: keyof SettingsModel, value: boolean) => {
        try {
            void settingsService.setKey(property, value);
        } catch (e) {
            setError(`Не удалось сохранить настройки: ${e}`);
        }
    };

    const settingsGroups = PopupSettingsConst;

    if (!settings) {
        if (error) {
            return <div className={'settings-message'}>{error}</div>;
        } else {
            return <div className={'settings-message'}>Загрузка параметров</div>;
        }
    } else {
        return (
            <div className={'settings'}>
                {settingsGroups.map((group, groupIndex) => (
                    <SettingsItemsGroup
                        key={groupIndex}
                        title={group.title}>
                        {group.items.map((value, index) => (
                            <SettingsItemComponent
                                title={value.title}
                                checked={settings[value.property] as boolean}
                                checkedChange={(checked) => updateItem(value.property, checked)}
                                key={index}
                            />
                        ))}
                    </SettingsItemsGroup>
                ))}
            </div>
        );
    }
}
