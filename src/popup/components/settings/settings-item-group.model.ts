import { SettingsItemModel } from './settings-item.model';

export interface SettingsItemGroupModel {
    readonly title: string;
    readonly items: readonly SettingsItemModel[];
}
