import { SettingsModel } from '../../../common/models/settings.model';

export interface SettingsItemModel {
    readonly title: string;
    readonly property: keyof SettingsModel;
}
