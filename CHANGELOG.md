# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.6.0](https://gitlab.com/kreoman/pikabu/compare/v1.5.0...v1.6.0) (2023-08-26)


### Features

* Добавлена возможность получения ника удаленного пользователя, если он был удален до того как был проиндексирован. ([8f005b0](https://gitlab.com/kreoman/pikabu/commit/8f005b076f5c439b75d51bd41388a3cd930c22a6))

## [1.5.0](https://gitlab.com/kreoman/pikabu/compare/v1.4.1...v1.5.0) (2023-08-25)


### Features

* Сохранение комментов ([49c9751](https://gitlab.com/kreoman/pikabu/commit/49c9751bacf06a869d6b0e3a83fe072d929b887b))


### Bug Fixes

* Исправлена работа индикатора даты регистрации пользователя в firefox ([16a38a0](https://gitlab.com/kreoman/pikabu/commit/16a38a08de2353da497356f2b8555cb21cbad4fe))

### [1.4.1](https://gitlab.com/kreoman/pikabu/compare/v1.4.0...v1.4.1) (2023-08-25)


### Bug Fixes

* Отображение индикатора пользователя в мобильной версии ([29fcd01](https://gitlab.com/kreoman/pikabu/commit/29fcd017da2d6ac3adeb6a4f30551f263e15ba83))

## [1.4.0](https://gitlab.com/kreoman/pikabu/compare/v1.3.1...v1.4.0) (2023-08-22)


### Features

* Сбор информации о постах и комментариях ([bc6ecff](https://gitlab.com/kreoman/pikabu/commit/bc6ecff0b4aedee897c02afb46b2d3ff6121a494))
* Сбор информации о рейтинге комментариев ([e7fef02](https://gitlab.com/kreoman/pikabu/commit/e7fef020d590665e6c5d94af344e436f4f395ca6))
* Сбор информации о рейтинге комментариев ([982472d](https://gitlab.com/kreoman/pikabu/commit/982472d848db3b288f8796c175237ae1ee553f92))
* Сбор информации о рейтинге постов ([b93dad0](https://gitlab.com/kreoman/pikabu/commit/b93dad0e7feae664ed685dc51667040bff8164bc))


### Bug Fixes

* Локальный url ([2c7ae97](https://gitlab.com/kreoman/pikabu/commit/2c7ae97539820b49ad335b03817841a0876bcc66))

### [1.3.1](https://gitlab.com/kreoman/pikabu/compare/v1.3.0...v1.3.1) (2023-08-21)


### Bug Fixes

* Исправление получения данных пользователя для firefox ([d33b079](https://gitlab.com/kreoman/pikabu/commit/d33b079067f42135a51b5d8f48c33167273886ad))

## [1.3.0](https://gitlab.com/kreoman/pikabu/compare/v1.2.1...v1.3.0) (2023-08-21)


### Features

* Добавлено получение пользователей из персонального кэша ([70cd81d](https://gitlab.com/kreoman/pikabu/commit/70cd81d7e515fee63c8675b973dd66eb36da7bde))

### [1.2.1](https://gitlab.com/kreoman/pikabu/compare/v1.2.0...v1.2.1) (2023-08-21)


### Bug Fixes

* Удален console.log ([908a7d2](https://gitlab.com/kreoman/pikabu/commit/908a7d2c92fcb5c71cb6a8070e2eaa8fae1bfbc9))

## [1.2.0](https://gitlab.com/kreoman/pikabu/compare/v1.1.1...v1.2.0) (2023-08-21)


### Features

* Получение информации о пользователе ([f78177c](https://gitlab.com/kreoman/pikabu/commit/f78177cad416b0bcd1982eb64f927cbfbacd472d))
* Реализовано отображение индикатора, указывающего на дату регистрации пользователя ([b2423ef](https://gitlab.com/kreoman/pikabu/commit/b2423efccb61573dfdd2a6dca16becd98ebdea48))

### [1.1.1](https://gitlab.com/kreoman/pikabu/compare/v1.1.0...v1.1.1) (2023-08-16)


### Bug Fixes

* Некорректно отображается рейтинг комментариев, если включена опция "показывать дополнительные посты под комментариями" ([1647bc4](https://gitlab.com/kreoman/pikabu/commit/1647bc46bbbd95d26965193c3c3129fe78aac2b8)), closes [#9](https://gitlab.com/kreoman/pikabu/issues/9)

## [1.1.0](https://gitlab.com/kreoman/pikabu/compare/v1.0.3...v1.1.0) (2023-08-16)


### Features

* добавлено отображение загрузки рейтинга комментариев ([a191930](https://gitlab.com/kreoman/pikabu/commit/a1919302bd1ddc3e720b9a54020046ce44179f6f))

### [1.0.3](https://gitlab.com/kreoman/pikabu/compare/v1.0.2...v1.0.3) (2023-08-15)


### Bug Fixes

* remove console.log ([2fc32db](https://gitlab.com/kreoman/pikabu/commit/2fc32dbeb67151c6598fdda5de091757bc5d2a4c))

### [1.0.2](https://gitlab.com/kreoman/pikabu/compare/v1.0.1...v1.0.2) (2023-08-15)


### Bug Fixes

* Для списка постов запришивались данные по всем страницам комментариев, что было избыточно ([cabe9e9](https://gitlab.com/kreoman/pikabu/commit/cabe9e903a63237f86903df2d8b4902351977a8c))
* Запрашивалась только первая 100 комментариев. Сейчас запрашиваются все. ([25d803d](https://gitlab.com/kreoman/pikabu/commit/25d803d9284ecda4a89cba5ce27ac523adc34c88))

### [1.0.1](https://gitlab.com/kreoman/pikabu/compare/v1.0.0...v1.0.1) (2023-08-15)


### Bug Fixes

* Дублировался блок с кастомным рейтингом коммента ([2b5c313](https://gitlab.com/kreoman/pikabu/commit/2b5c3131c7b740bc48b1fa42319f3524ca0478f8))

## [1.0.0](https://gitlab.com/kreoman/pikabu/compare/v0.0.35...v1.0.0) (2023-08-15)


### Bug Fixes

* Исправлено отображение рейтинга у своих комментариев ([fb92d6d](https://gitlab.com/kreoman/pikabu/commit/fb92d6d5256a8430629d297a972c8136674b35d9))

### [0.0.35](https://gitlab.com/kreoman/pikabu/compare/v0.0.34...v0.0.35) (2023-08-13)


### Features

* Собираем user-agent для статистики по браузерам ([e39e29c](https://gitlab.com/kreoman/pikabu/commit/e39e29c4c5489d1694d4745d3813d779dde752ce))

### [0.0.34](https://gitlab.com/kreoman/pikabu/compare/v0.0.33...v0.0.34) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) remove content_security_policy ([52ba202](https://gitlab.com/kreoman/pikabu/commit/52ba2027e32512e06ada5879ef40f89c00c06a57))

### [0.0.33](https://gitlab.com/kreoman/pikabu/compare/v0.0.32...v0.0.33) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) ([e99b1c7](https://gitlab.com/kreoman/pikabu/commit/e99b1c726bd055354d578fb44172091f0b51e182))

### [0.0.32](https://gitlab.com/kreoman/pikabu/compare/v0.0.31...v0.0.32) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) ([b7e2c44](https://gitlab.com/kreoman/pikabu/commit/b7e2c4457a292638b801b35d693b456fcc2d5552))

### [0.0.31](https://gitlab.com/kreoman/pikabu/compare/v0.0.30...v0.0.31) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) ([eb3f98a](https://gitlab.com/kreoman/pikabu/commit/eb3f98ad81a8a7bb058c3dc525506d012aa14792))

### [0.0.30](https://gitlab.com/kreoman/pikabu/compare/v0.0.29...v0.0.30) (2023-08-13)


### Features

* Косметические изменения применяются глобально ко всей странице ([7d9b485](https://gitlab.com/kreoman/pikabu/commit/7d9b485a05382dbfbfdcf97fe86b19bec900bf09))

### [0.0.29](https://gitlab.com/kreoman/pikabu/compare/v0.0.28...v0.0.29) (2023-08-13)


### Features

* Добавлено динамическое включение/выключение рейтинга в мобильной версии ([f9f1616](https://gitlab.com/kreoman/pikabu/commit/f9f161699956b1cc3628938f0548a6f8a6f571f3))

### [0.0.28](https://gitlab.com/kreoman/pikabu/compare/v0.0.27...v0.0.28) (2023-08-13)


### Features

* **popup:** settings in popup ([dd9ec9f](https://gitlab.com/kreoman/pikabu/commit/dd9ec9f3cbcae40da323b59f4952e85bb1627b8f))
* Добавлено меню ([4a1ddb6](https://gitlab.com/kreoman/pikabu/commit/4a1ddb6be4ba0b51bfa907104d3391eb963130e1))
* Добавлены косметические опции: скрытие доната и скрытие дизайна коротких постов ([43ee5c2](https://gitlab.com/kreoman/pikabu/commit/43ee5c2584229c46de5d61ed5a889c4f4ca24b11))


### Bug Fixes

* Убрано мерцание рейтинга при подгрузке новых постов ([052c751](https://gitlab.com/kreoman/pikabu/commit/052c75107c1e5c57608c63d54929304064ecc20d))

### [0.0.27](https://gitlab.com/kreoman/pikabu/compare/v0.0.26...v0.0.27) (2023-08-13)

### [0.0.26](https://gitlab.com/kreoman/pikabu/compare/v0.0.25...v0.0.26) (2023-08-11)


### Bug Fixes

* Ловим ошибки по каждому потоку, чтоб не ломать общий ([c15cbc3](https://gitlab.com/kreoman/pikabu/commit/c15cbc346432178a1a5b4c54dc128fb1b484c050))

### [0.0.25](https://gitlab.com/kreoman/pikabu/compare/v0.0.24...v0.0.25) (2023-08-10)

### [0.0.24](https://gitlab.com/kreoman/pikabu/compare/v0.0.23...v0.0.24) (2023-08-10)


### Features

* Добавлен сбор статистики использования и сведений об ошибках ([8abbc7b](https://gitlab.com/kreoman/pikabu/commit/8abbc7bbb6c6f86d8ec4ab2beec4630c271b3b89))
* Убрал замену "На три месяца" на "Навсегда", так как разрабы соизволили сами вернуть этот пункт в меню ([bd752b1](https://gitlab.com/kreoman/pikabu/commit/bd752b1512585869bbe13bcdeb0a51e2f0be4983))

### [0.0.23](https://gitlab.com/kreoman/pikabu/compare/v0.0.22...v0.0.23) (2023-08-08)


### Features

* Добавлена поддержка мобильной версии ([b3770d5](https://gitlab.com/kreoman/pikabu/commit/b3770d575932ddc01f223d08a8fc27893756f2c8))
* Добавлена поддержка мобильной версии ([fe44f30](https://gitlab.com/kreoman/pikabu/commit/fe44f303555b5179a78071fdfccdc0ab988a6f87))

### [0.0.22](https://gitlab.com/kreoman/pikabu/compare/v0.0.21...v0.0.22) (2023-08-08)


### Bug Fixes

* Забыт стиль для story-vote ([4b41be3](https://gitlab.com/kreoman/pikabu/commit/4b41be39076f73ec3861f835a36e408d73572180))

### [0.0.21](https://gitlab.com/kreoman/pikabu/compare/v0.0.20...v0.0.21) (2023-08-08)


### Features

* Сборка дистрибутива в production mode ([a1848f9](https://gitlab.com/kreoman/pikabu/commit/a1848f9436d10ae72453cf110be665c75b3cc6ac))

### [0.0.20](https://gitlab.com/kreoman/pikabu/compare/v0.0.19...v0.0.20) (2023-08-08)


### Features

* Добавлена обработка ситуации со скрытым рейтингом, когда пост был создан менее часа назад. В таком случае отображается серая шкала с хинтом. ([dddceca](https://gitlab.com/kreoman/pikabu/commit/dddceca9cfbece062390715ce86f66904ba17eb6))

### [0.0.19](https://gitlab.com/kreoman/pikabu/compare/v0.0.18...v0.0.19) (2023-08-07)


### Features

* Изменено отображение минусов и плюсов поста. Теперь используется верстка сайта со шкалой. ([1d825a9](https://gitlab.com/kreoman/pikabu/commit/1d825a94bb3e614a1c41c4b5aa1b5df1c096eb9f))

### [0.0.18](https://gitlab.com/kreoman/pikabu/compare/v0.0.17...v0.0.18) (2023-08-07)


### Bug Fixes

* При обновлении (раскрытии) комментариев дублировался блок с рейтингом поста ([9546a7a](https://gitlab.com/kreoman/pikabu/commit/9546a7ac7b3b0f322b5f348de00c4ce2cbcd77b6))

### [0.0.17](https://gitlab.com/kreoman/pikabu/compare/v0.0.16...v0.0.17) (2023-08-07)


### Bug Fixes

* Добавлена обработка ошибок при получении информации о рейтинге ([1cc559f](https://gitlab.com/kreoman/pikabu/commit/1cc559fc8a6674c64bab974354cdd033c05c571a))

### [0.0.16](https://gitlab.com/kreoman/pikabu/compare/v0.0.15...v0.0.16) (2023-08-07)


### Bug Fixes

* Добавлена обработка ошибок при получении информации о рейтинге ([661503d](https://gitlab.com/kreoman/pikabu/commit/661503d590f807622ecd0a6722221ae5b7abfc6c))

### [0.0.15](https://gitlab.com/kreoman/pikabu/compare/v0.0.14...v0.0.15) (2023-08-07)

### [0.0.14](https://gitlab.com/kreoman/pikabu/compare/v0.0.13...v0.0.14) (2023-08-06)


### Bug Fixes

* Скрыл недоработанное меню ([2b97c16](https://gitlab.com/kreoman/pikabu/commit/2b97c1630337b9123ce70322742d435639dd499d))

### [0.0.13](https://gitlab.com/kreoman/pikabu/compare/v0.0.12...v0.0.13) (2023-08-06)


### Features

* Добавлены минусы ([82067b3](https://gitlab.com/kreoman/pikabu/commit/82067b360e490d33e7f3754e7414fb464e429e03))

### [0.0.12](https://gitlab.com/kreoman/pikabu/compare/v0.0.11...v0.0.12) (2023-08-03)

### [0.0.11](https://gitlab.com/kreoman/pikabu/compare/v0.0.10...v0.0.11) (2023-08-03)

### [0.0.10](https://gitlab.com/kreoman/pikabu/compare/v0.0.9...v0.0.10) (2023-08-03)

### [0.0.9](https://gitlab.com/kreoman/pikabu/compare/v0.0.8...v0.0.9) (2023-08-03)

### [0.0.8](https://gitlab.com/kreoman/pikabu/compare/v0.0.7...v0.0.8) (2023-08-03)

### [0.0.7](https://gitlab.com/kreoman/pikabu/compare/v0.0.6...v0.0.7) (2023-08-03)

### [0.0.6](https://gitlab.com/kreoman/pikabu/compare/v0.0.5...v0.0.6) (2023-08-03)

### [0.0.5](https://gitlab.com/kreoman/pikabu/compare/v0.0.4...v0.0.5) (2023-08-03)

### [0.0.4](https://gitlab.com/kreoman/pikabu/compare/v0.0.3...v0.0.4) (2023-08-03)

### 0.0.3 (2023-08-03)


### Features

* Добавлена кнопка быстрого бана автора поста ([27f8fff](https://gitlab.com/kreoman/pikabu/commit/27f8fff39f9dd8e68909b39bff82a24e150d0cba))


### Bug Fixes

* Модификация меню блокировки работает не только для пользователя, но и для тегов ([dd73ddc](https://gitlab.com/kreoman/pikabu/commit/dd73ddcfcebb697b61a6c0f53598dd808330e145))
