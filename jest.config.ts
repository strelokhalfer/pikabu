export default {
    preset: 'jest-preset-typescript',
    roots: ['<rootDir>/src/'],
    // testMatch: ['**/+(*.)+(spec|test).+(ts)'],
    setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
    collectCoverage: true,
    coverageReporters: ['html'],
    coverageDirectory: 'coverage/my-app',
    transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
};
